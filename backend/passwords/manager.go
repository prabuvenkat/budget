package passwords

import (
	"errors"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"

	"budget/global"
)

/*
############################################################################

README

This module is used for generating usernames and passwords.

1. Edit secrets/_UserPasswordDefinition.txt to specify what you need.

2. Set STAGE = 1 in budget.go and run budget. The result is:
	[1] users and passwords are created/updated in the budget database
	[2] secrets/[filename].sh is created
	[3] secrets/[filename].txt is created

3. Set STAGE = 2 in budget.go and run budget. Now budget is up and running.

4. Use secrets/[filename].sh [2] to generate login QR-codes for users:
 	- on iMac open terminal and run "sh [filename].sh".
	- this will create [username].png files with pictures of QR-codes.

5. Use secrets/[filename].txt [3] and the QR-codes to communicate passwords.

############################################################################
*/

const (
	DEFAULT_INPUT_FILE          string = "secrets/_UserPasswordDefinition.txt"
	DEFAULT_OUTPUT_FILE_NAME    string = "_DefaultOutput"
	DEFAULT_PASSWORD_DEFINITION string = "ULLD__MULD"
)

type UserPwd struct {
	Name     string
	Password string
	Hash     string
	Status   bool
}

func GenerateUsersAndPasswords() ([]UserPwd, string, bool, error) {
	global.MyPrintTrace("START ...")

	rand.Seed(time.Now().UTC().UnixNano())

	var err error = nil
	up, outputFileName, update := generateUsersAndPasswordsFromFile(DEFAULT_INPUT_FILE)

	if global.FileExists(outputQRfile(outputFileName)) || global.FileExists(outputPWfile(outputFileName)) {
		err = errors.New("Error: Output password files (" + outputFileName + ") already exist!")
	}

	global.MyPrintTrace("FINISH")

	return up, outputFileName, update, err
}

func outputQRfile(outputFileName string) string {
	return "secrets/" + outputFileName + ".sh"
}

func outputPWfile(outputFileName string) string {
	return "secrets/" + outputFileName + ".txt"
}

func CreatePasswordFiles(outputFileName string, up []UserPwd) error {
	global.MyPrintTrace("START ... " + outputFileName)
	var qrFile string = outputQRfile(outputFileName)
	var pwFile string = outputPWfile(outputFileName)

	if global.FileExists(qrFile) || global.FileExists(pwFile) {
		global.MyPrintTrace("ERROR: files exsist! " + qrFile + " " + pwFile)
		return errors.New("Error: files already exist!")
	}

	f1, _ := os.Create(qrFile)
	fmt.Fprintln(f1, "# !!! THIS FILE IS GENERATED !!!")
	fmt.Fprintln(f1, " ")
	fmt.Fprintln(f1, "# Use this file to generate login QR-codes for users.")
	fmt.Fprintln(f1, "# On iMac open terminal and run [sh filename.sh].")
	fmt.Fprintln(f1, "# This creates [Username].png files with QR-code images.")
	fmt.Fprintln(f1, " ")
	fmt.Fprintln(f1, "rm *.png")
	fmt.Fprintln(f1, " ")

	f2, _ := os.Create(pwFile)
	fmt.Fprintln(f2, "# !!! THIS FILE IS GENERATED !!!")
	fmt.Fprintln(f2, " ")
	fmt.Fprintln(f2, "# Use this file to communicate passwords to users. ")
	fmt.Fprintln(f2, " ")
	fmt.Fprintln(f2, "Username Password")
	fmt.Fprintln(f2, "-----------------")

	for _, u := range up {
		if u.Status {
			_, _ = fmt.Fprintln(f1, "qrencode -o "+u.Name+".png "+u.Name+"###***###"+u.Password)
			_, _ = fmt.Fprintln(f2, u.Name+" "+u.Password)
		} else {
			_, _ = fmt.Fprintln(f1, "# ERROR: user not created - "+u.Name)
			_, _ = fmt.Fprintln(f2, "# ERROR: user not created - "+u.Name)
		} 
	}

	fmt.Fprintln(f1, " ")
	fmt.Fprintln(f1, "# !!! THIS FILE IS GENERATED !!!")

	fmt.Fprintln(f2, " ")
	fmt.Fprintln(f2, "# !!! THIS FILE IS GENERATED !!!")

	global.MyPrintTrace("created " + f1.Name())
	global.MyPrintTrace("created " + f2.Name())

	_ = f1.Close()
	_ = f2.Close()

	global.MyPrintTrace("FINISH ")

	return nil
}

func generateUsersAndPasswordsFromFile(passwordDefinitionFile string) ([]UserPwd, string, bool) {
	global.MyPrintTrace("START ... " + passwordDefinitionFile)

	var outputFileName string = DEFAULT_OUTPUT_FILE_NAME

	if !global.FileExists(passwordDefinitionFile) {
		global.MyPrintTrace("ERROR definition file does not exist - " + passwordDefinitionFile)
		createDefinitionFile(passwordDefinitionFile)
		var empty []UserPwd
		return empty, outputFileName, false
	}

	var up []UserPwd
	var passwordDefinition string = DEFAULT_PASSWORD_DEFINITION
	var update bool = false

	lines, _ := global.ReadLines(passwordDefinitionFile)
	for _, line := range lines {
		line = strings.Replace(line, " ", "", -1)
		if len(line) >= 4 {
			key := line[0:4]
			value := line[4:len(line)]
			switch key {
			case "upd=":
				if value == "false" {
					global.MyPrintTrace(key + value)
				} else if value == "true" {
					global.MyPrintTrace(key + value)
					update = true
				} else {
					global.MyPrintTrace(key + value + " !!! ERROR, default false applied, ignored line !!!")
				}
			case "run=":
				if len(value) >= 4 {
					global.MyPrintTrace(key + value)
					outputFileName = value
				} else {
					global.MyPrintTrace(key + value + " !!! ERROR, run length < 4, ignored line !!!")
				}
			case "pwd=":
				if len(value) >= 8 {
					global.MyPrintTrace(key + value)
					passwordDefinition = value
				} else {
					global.MyPrintTrace(key + value + " !!! ERROR, password length < 8, ignored line !!!")
				}
			case "set=":
				set := strings.Split(value, ",")
				if len(set) == 2 {
					nnn, err := strconv.Atoi(set[1])
					if err == nil {
						global.MyPrintTrace(key + value)
						up = append(up, generateSet(nnn, set[0], passwordDefinition)...)
					} else {
						global.MyPrintTrace(key + value + " !!! ERROR, not a number, ignored line !!!")
					}
				} else {
					global.MyPrintTrace(key + value + " !!! ERROR, [prefix],[number] expected, ignored line !!!")
				}
			case "one=":
				global.MyPrintTrace(key + value)
				up = append(up, generateUser(value, passwordDefinition))
			}
		}
	}

	global.MyPrintTrace("FINISH")
	return up, outputFileName, update
}

func generateSet(number int, prefix string, passwordDefinition string) []UserPwd {
	var up []UserPwd
	var user string
	for i := 0; i < number; i++ {
		user = prefix + strconv.Itoa(1000 + i)[1:4]
		up = append(up, generateUser(user, passwordDefinition))
	}
	return up
}

func generateUser(user string, passwordDefinition string) UserPwd {
	pwd := CreatePassword(passwordDefinition)
	hash, _ := bcrypt.GenerateFromPassword([]byte(pwd), 8)
	return UserPwd{user, pwd, string(hash), true}
}

func createDefinitionFile(passwordDefinitionFile string) {

	global.MyPrintTrace("START ... " + passwordDefinitionFile)

	f1, _ := os.Create(passwordDefinitionFile)

	fmt.Fprintln(f1, "##########################################################################")
	fmt.Fprintln(f1, "#")
	fmt.Fprintln(f1, "# This file (_UserPasswordDefinition.txt) is used for generating passwords for users")
	fmt.Fprintln(f1, "#")
	fmt.Fprintln(f1, "# All spaces are removed from each line before processing.")
	fmt.Fprintln(f1, "# Lines that start with 'pwd=', 'set=', 'one=' or 'run=' are processed, all other lines are ignored.")
	fmt.Fprintln(f1, "#")
	fmt.Fprintln(f1, "# pwd=[string]          defines the password definition (D=digit, U=uppercase, L=lowercase, M=mark, _=D|U|L|M) ")
	fmt.Fprintln(f1, "# set=[prefix],[number] defines a set of users (username = [prefix][nnn], 0 <= nnn < [number] <= 1000)")
	fmt.Fprintln(f1, "# one=[username]        defines one user")
	fmt.Fprintln(f1, "# run=[filename]        defines the unique name for a password generation run.  ")
	fmt.Fprintln(f1, "# upd=true|false        defines if existing passwords should be updated (upd=true), default = false.") 
	fmt.Fprintln(f1, "# ")
	fmt.Fprintln(f1, "# If no password definition is defined the default is used (pwd=ULLD__MULD).")
	fmt.Fprintln(f1, "# The length of a password definition shoud be >= 8.")
	fmt.Fprintln(f1, "# A password definition is used until a new password definition is defined.")
	fmt.Fprintln(f1, "#")
	fmt.Fprintln(f1, "# The filename defined by 'run=' should be unique in order not to overwrite output files.")
	fmt.Fprintln(f1, "# Each run results in two files [filename].sh and [filename].txt with generated passwords.")
	fmt.Fprintln(f1, "#")
	fmt.Fprintln(f1, "##########################################################################")
	fmt.Fprintln(f1, "")
	fmt.Fprintln(f1, "# define new password definition")
	fmt.Fprintln(f1, "pwd=ULLD__MULD")
	fmt.Fprintln(f1, "")
	fmt.Fprintln(f1, "# define sets of users")
	fmt.Fprintln(f1, "set=example_,100")
	fmt.Fprintln(f1, "")
	fmt.Fprintln(f1, "# define one user")
	fmt.Fprintln(f1, "one=someone")
	fmt.Fprintln(f1, "")
	fmt.Fprintln(f1, "# define unique run")
	fmt.Fprintln(f1, "run=19010101_1")
	fmt.Fprintln(f1, "")
	fmt.Fprintln(f1, "# define update all passwords")
	fmt.Fprintln(f1, "#upd=true")

	_ = f1.Close()

	global.MyPrintTrace("FINISH ")
}
