package global

const (
	TBL_USER        	string = "tbl_user"
	TBL_PHONE       	string = "tbl_phone"
	TBL_SYNC        	string = "tbl_sync"
	TBL_TRANSACTION 	string = "tbl_receipt_transaction"
	TBL_PRODUCT     	string = "tbl_receipt_product"
	TBL_IMAGE       	string = "tbl_receipt_image"
	TBL_SEARCH_PRODUCT 	string = "tbl_search_suggestions_products"
	TBL_SEARCH_STORE   	string = "tbl_search_suggestions_stores"
)

const (
	CREATE int = 0
	UPDATE int = 1
	DELETE int = 2
)

const (
	RECEIPT  	string = "receipt"
	PRODUCT 	string = "product"
	STORE 		string = "store"
)

const (
	RECEIPT_TYPE  	int = 1
	PRODUCT_TYPE 	int = 2
	STORE_TYPE 		int = 3
)

