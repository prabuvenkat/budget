package global

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"runtime"
)

func DataType(data string) (int, error) {
	var dataType int

	switch data {
	case RECEIPT:
		dataType = RECEIPT_TYPE
	case PRODUCT:
		dataType = PRODUCT_TYPE
	case STORE:
		dataType = STORE_TYPE
	default:
		dataType = 0
	}

	if dataType != 0 {
		return dataType, nil
	} else {
		return dataType, errors.New("Error: DataType does not exist!")
	}
}

func MyPrint(text string) {
	if true {
		if text == "#" {
			fmt.Println("################################################################")
		} else {
			fmt.Println(text)
		}
	}
}

func GetEnvironment(item string, stopOnMissingEnvironment bool) string {
	var env string

	if env = os.Getenv(item); len(env) == 0 {

		if stopOnMissingEnvironment {
			MyPrint("Error: Environment " + item + " not set!")
			os.Exit(1)
		}

		switch item {
		case "PORT":
			env = "8000"
		case "BUDGET_HOST":
			env = "localhost"
		case "BUDGET_DBNAME":
			env = "budget"
		case "BUDGET_PORT":
			env = "5432"
		case "BUDGET_USER":
			env = "postgres"
		case "BUDGET_PASSWORD":
			env = "o0i9a3r5f6e4w3"
		default:
			env = ""
		}

		if env == "" {
			MyPrint("Error: Environment not set, no default available: " + item)
			os.Exit(1)
		} else {
			MyPrint("Warning: Environment not set, default used: " + item)
		}
	}

	return env
}

func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func ReadLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func Trace() (string, int, string) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		return "?", 0, "?"
	}

	fn := runtime.FuncForPC(pc)
	return file, line, fn.Name()
}

func MyPrintTrace(text string) {
	var trace string = ""

	pc, _, _, ok := runtime.Caller(1)
	if ok {
		fn := runtime.FuncForPC(pc)
		trace = fn.Name() + ":"
	}

	if true {
		if text == "#" {
			fmt.Println("################################################################")
		} else {
			fmt.Println(trace, text)
		}
	}
}
