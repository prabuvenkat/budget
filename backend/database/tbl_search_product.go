package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"budget/global"
)

func createSearchProduct(tx *sql.Tx, sync_id int64, s *global.SearchProduct) error {
	sqlStatement := "" +
		"INSERT INTO " + global.TBL_SEARCH_PRODUCT + " (sync_id, product, productCategory, productCode, " +
		"	lastAdded, count) " +
		"VALUES ($1, $2, $3, $4, $5, $6) "

	_, err = tx.Exec(sqlStatement, sync_id, s.Product, s.ProductCategory, s.ProductCode, s.LastAdded, s.Count)
	if err != nil {
		return err
	}

	global.MyPrint("& Inserted SearchProduct")
	return nil
}

func deleteSearchProduct(tx *sql.Tx, sync_id int64) error {
	sqlStatement := "" +
		"DELETE FROM " + global.TBL_SEARCH_PRODUCT + " " +
		"WHERE sync_id=$1 "

	_, err = tx.Exec(sqlStatement, sync_id)
	if err != nil {
		return err
	}
	
	global.MyPrint("& Deleted SearchProduct")
	return nil
}

func existsSearchProduct(sync_id int64) (bool, error) {
	var exists bool

	sqlStatement := "" +
		"SELECT EXISTS " +
		"(SELECT sync_id FROM " + global.TBL_SEARCH_PRODUCT + " WHERE sync_id=$1) "

	err := con.QueryRow(sqlStatement, sync_id).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}
	return exists, nil
}

func scanSearchProductRow(rows *sql.Rows) global.SearchProduct {
	var sync_id int64
	var product string
	var productCategory string
	var productCode string
	var lastAdded int
	var count int
	err = rows.Scan(&sync_id, &product, &productCategory, &productCode, &lastAdded, &count)
	if err != nil {
		panic(err)
	}
	return global.SearchProduct{
		Product:      		product,
		ProductCategory:    productCategory,
		ProductCode:        productCode,
		LastAdded:          lastAdded,
		Count:     			count}
}

func selectSearchProduct(sync_id int64) (global.SearchProduct, error) {
	var searchProduct global.SearchProduct

	sqlStatement := "" +
		"SELECT sync_id, product, productCategory, productCode, " +
		"	lastAdded, count " +
		"FROM " + global.TBL_SEARCH_PRODUCT + " " +
		"WHERE sync_id=$1 "

	rows, err := con.Query(sqlStatement, sync_id)
	if err != nil {
		return searchProduct, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		searchProduct = scanSearchProductRow(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return searchProduct, err
	}

	if found {
		return searchProduct, nil
	} else {
		return searchProduct, errors.New("Error: SearchProduct not found!")
	}
}
