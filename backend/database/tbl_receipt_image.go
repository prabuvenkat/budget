package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"budget/global"
)

func createImage(tx *sql.Tx, sync_id int64, i *global.Image) error {
	sqlStatement := "" +
		"INSERT INTO " + global.TBL_IMAGE + " (sync_id, transaction_id, image) " +
		"VALUES ($1, $2, $3) "

	_, err = tx.Exec(sqlStatement, sync_id, i.TransactionID, i.Base64image)
	if err != nil {
		return err
	}

	global.MyPrint("& Inserted Image")
	return nil
}

func deleteImage(tx *sql.Tx, sync_id int64) error {
	sqlStatement := "" +
		"DELETE FROM " + global.TBL_IMAGE + " " +
		"WHERE sync_id=$1 "

	_, err = tx.Exec(sqlStatement, sync_id)
	if err != nil {
		return err
	}

	global.MyPrint("& Deleted Image")
	return nil
}

func existsImage(sync_id int64) (bool, error) {
	var exists bool

	sqlStatement := "" +
		"SELECT EXISTS " +
		"(SELECT sync_id FROM " + global.TBL_IMAGE + " WHERE sync_id=$1) "

	err := con.QueryRow(sqlStatement, sync_id).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}
	return exists, nil
}

func scanImageRow(rows *sql.Rows) global.Image {
	var sync_id int64
	var transactionID string
	var base64image string
	err = rows.Scan(&sync_id, &transactionID, &base64image)
	if err != nil {
		panic(err)
	}
	return global.Image{
		TransactionID:      transactionID,
		Base64image:        base64image}
}

func selectImage(sync_id int64) (global.Image, error) {
	var image global.Image

	sqlStatement := "" +
		"SELECT sync_id, transaction_id, image " +
		"FROM " + global.TBL_IMAGE + " " +
		"WHERE sync_id=$1 "

	rows, err := con.Query(sqlStatement, sync_id)
	if err != nil {
		return image, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		image = scanImageRow(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return image, err
	}

	if found {
		return image, nil
	} else {
		return image, errors.New("Error: Image not found!")
	}
}
