package database

import (
	"database/sql"

	_ "github.com/lib/pq"

	"fmt"
)

var con *sql.DB
var err error

func Init(host string, port int, user string, password string, dbname string) {
	connectDatabase(host, port, user, password, dbname)
}

func connectDatabase(host string, port int, user string, password string, dbname string) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	con, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	//defer DBCon.Close()

	err = con.Ping()
	if err != nil {
		panic(err)
	}
}
