import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart';

import 'package:budget_onderzoek/database/datamodels/sync_db.dart';
import 'package:budget_onderzoek/database/datamodels/sync_types.dart';
import 'package:budget_onderzoek/database/datamodels/transaction_db.dart';
import 'package:budget_onderzoek/database/datamodels/search_suggestion_db.dart';
import 'package:path_provider/path_provider.dart';

const BACKENDISACTIVE = false;
const URL = "http://172.16.37.446:8000/budget/";
//const URL = "http://192.168.178.16:8000/budget/";
//const URL = "http://cfdemobudget.cfapps.io/budget/";

class Synchronise {
  static bool backendIsActive() {
    return BACKENDISACTIVE;
  }

  static void synchronise() async {
    if (!BACKENDISACTIVE) {
      return;
    }
    print("synchronise");

    SyncId syncId = await SyncDatabase.getSyncId();

    // try {
    //   SyncUser syncUser = await registerUser("XX_000", "Ntj0=Ha0");
    //   if (syncUser.id != 0) {
    //     syncId.userGuid = syncUser.guid;
    //     await SyncDatabase.updateSyncId(syncId);
    //   }

    //   if (syncId.userGuid != "") {
    //     SyncPhone syncPhone =
    //         await registerPhone(syncId.userGuid, syncId.phoneName);
    //     if (syncPhone.id != 0) {
    //       syncId.phoneGuid = syncPhone.guid;
    //       await SyncDatabase.updateSyncId(syncId);
    //     }
    //   }
    // } catch (e) {
    //   print(e.toString());
    //   return;
    // }

    if (syncId.userGuid != "" && syncId.phoneGuid != "") {
      await pullReceiptData(syncId);
      await pullSearchProductData(syncId);
      await pullSearchStoreData(syncId);
      await pushData(syncId);
    }
  }

  static String dataName(int dataType) {
    String name = "";
    switch (dataType) {
      case GLOBAL_TYPE:
        name = "global";
        break;
      case RECEIPT_TYPE:
        name = "receipt";
        break;
      case PRODUCT_TYPE:
        name = "product";
        break;
      case STORE_TYPE:
        name = "store";
        break;
      default:
        name = "";
    }
    return name;
  }

  static Future<void> pullReceiptData(SyncId syncId) async {
    int noInserted = 0;
    int noUpdated = 0;
    int noNeglected = 0;

    bool act = true;

    while (act) {
      int update = await SyncDatabase.getSyncUpdate(RECEIPT_TYPE);
      SyncReceiptData syncData = await getNextReceiptDataToPull(syncId, update);
      if (syncData.sync.dataIdentifier == "") {
        act = false;
      } else {
        if (syncData.transaction.receiptLocation != "") {
          String image = imageName(syncData.transaction.receiptLocation);
          syncData.transaction.receiptLocation = await imagePath(image);
          print("imagePath: " + syncData.transaction.receiptLocation);
        }

        update = syncData.sync.update;
        syncData.sync.update = 0;
        Sync oldSync = await SyncDatabase.getSync(
            RECEIPT_TYPE, syncData.sync.dataIdentifier);
        if (oldSync.dataIdentifier == "") {
          noInserted = noInserted + 1;
          await SyncDatabase.createImportedSync(syncData.sync);
          if (syncData.sync.action != DELETED) {
            await SyncDatabase.insertTransaction(syncData.transaction);
            syncData.products.forEach((prod) {
              SyncDatabase.insertProduct(prod);
            });
          }
          if (syncData.transaction.receiptLocation != "") {
            var imageBytes = base64.decode(syncData.image.base64image);
            File file = File(syncData.transaction.receiptLocation);
            await file.writeAsBytes(imageBytes);
          }
        } else {
          if (oldSync.timestamp < syncData.sync.timestamp) {
            noUpdated = noUpdated + 1;
            SyncDatabase.updateImportedSync(syncData.sync);
            await TransactionDatabase.deleteTransaction(
                syncData.sync.dataIdentifier);
            if (syncData.sync.action != DELETED) {
              await SyncDatabase.insertTransaction(syncData.transaction);
              syncData.products.forEach((prod) {
                SyncDatabase.insertProduct(prod);
              });
              if (syncData.transaction.receiptLocation != "") {
                var imageBytes = base64.decode(syncData.image.base64image);
                File file = File(syncData.transaction.receiptLocation);
                await file.writeAsBytes(imageBytes);
              }
            }
          } else {
            noNeglected = noNeglected + 1;
          }
        }
        SyncDatabase.updateSyncUpdate(RECEIPT_TYPE, update);
      }
    }

    print('pullReceiptData: inserted=' +
        noInserted.toString() +
        ' updated=' +
        noUpdated.toString() +
        ' neglected=' +
        noNeglected.toString());
  }

  static Future<void> pushData(SyncId syncId) async {
    int pushed = 0;
    int lastPushed = -1;
    int attempt = 0;

    bool act = true;

    while (act) {
      SyncPhone syncPhone =
          await getLastPhoneUpdate(syncId.userGuid, syncId.phoneGuid);
      Sync sync = await SyncDatabase.getNextSyncToPush(syncPhone.update);

      if (lastPushed != sync.update) {
        lastPushed = sync.update;
        attempt = 1;
      } else if (attempt < 5) {
        attempt = attempt + 1;
        sleep(Duration(milliseconds: 100));
      } else {
        break;
      }

      if (sync.dataIdentifier == "") {
        act = false;
      } else {
        print("Push attempt: " + attempt.toString());
        pushed = pushed + 1;
        Sync sSync = new Sync(sync.dataType, sync.dataIdentifier, sync.update,
            sync.timestamp, sync.action);

        if (sync.dataType == RECEIPT_TYPE) {
          print("RECEIPT_TYPE");
          SyncTransaction sTrans;
          List<SyncProduct> sProds = new List<SyncProduct>();
          SyncImage sImage;

          if (sSync.action != DELETED) {
            List<Map<String, dynamic>> trans =
                await TransactionDatabase.queryTransaction(sync.dataIdentifier);
            sTrans = SyncTransaction.fromQuerys(trans[0]);

            List<Map<String, dynamic>> prods =
                await TransactionDatabase.queryTransactionProducts(
                    sync.dataIdentifier);

            prods.forEach((p) {
              sProds.add(new SyncProduct.fromQuery(p));
            });

            sImage = SyncImage(sTrans.transactionID, "");
            if (sTrans.receiptLocation != "") {
              File imageFile = new File(sTrans.receiptLocation);
              List<int> imageBytes = imageFile.readAsBytesSync();
              sImage.base64image = base64.encode(imageBytes);

              sTrans.receiptLocation = imageName(sTrans.receiptLocation);
            }
          } else {
            sTrans = SyncTransaction(sync.dataIdentifier, "", "", 0, "", "", "",
                "", 0.0, "", "", "");
            sImage = SyncImage(sTrans.transactionID, "");
          }

          SyncReceiptData syncData =
              new SyncReceiptData(sSync, sTrans, sProds, sImage);

          String url = dataUrl(syncId.userGuid, syncId.phoneGuid,
              dataName(sync.dataType), sync.update);

          Response response = await post(url, body: jsonEncode(syncData));
        } else if (sync.dataType == PRODUCT_TYPE) {
          print("PRODUCT_TYPE");
          SyncSearchProduct sSearchProd;

          if (sSync.action != DELETED) {
            List<Map<String, dynamic>> searchProd =
                await SearchSuggestions.querySearchProduct(sync.dataIdentifier);
            sSearchProd = SyncSearchProduct.fromQuerys(searchProd[0]);
          } else {
            sSearchProd = SyncSearchProduct("", "", sync.dataIdentifier, 0, 0);
          }

          SyncSearchProductData syncData =
              new SyncSearchProductData(sSync, sSearchProd);

          String url = dataUrl(syncId.userGuid, syncId.phoneGuid,
              dataName(sync.dataType), sync.update);

          Response response = await post(url, body: jsonEncode(syncData));
        } else if (sync.dataType == STORE_TYPE) {
          print("STORE_TYPE");
          SyncSearchStore sSearchStore;

          if (sSync.action != DELETED) {
            List<Map<String, dynamic>> searchStore =
                await SearchSuggestions.querySearchStore(sync.dataIdentifier);
            sSearchStore = SyncSearchStore.fromQuerys(searchStore[0]);
          } else {
            sSearchStore = SyncSearchStore(sync.dataIdentifier, "", 0, 0);
          }

          SyncSearchStoreData syncData =
              new SyncSearchStoreData(sSync, sSearchStore);

          String url = dataUrl(syncId.userGuid, syncId.phoneGuid,
              dataName(sync.dataType), sync.update);

          Response response = await post(url, body: jsonEncode(syncData));
          print(response.body);
        }
      }
    }

    print("pushData: pushed=" + pushed.toString());
  }

  static Future<SyncUser> registerUser(String user, String password) async {
    String url = URL + 'user/' + user + '/' + password;
    Response response = await post(url);

    print('registerUser');
    print(response.body);

    SyncUser syncUser = SyncUser.fromJson(json.decode(response.body));

    return syncUser;
  }

  static Future<SyncPhone> registerPhone(
      String userGuid, String phoneName) async {
    String url = URL + 'phone/' + userGuid + '/' + phoneName;
    Response response = await post(url);

    print('registerPhone');
    print(response.body);

    SyncPhone syncPhone = SyncPhone.fromJson(json.decode(response.body));
    return syncPhone;
  }

  static Future<SyncPhone> getLastPhoneUpdate(
      String userGuid, String phoneGuid) async {
    String url = URL + 'phone/' + userGuid + '/' + phoneGuid;
    Response response = await get(url);

    SyncPhone syncPhone = SyncPhone.fromJson(json.decode(response.body));
    return syncPhone;
  }

  static Future<SyncReceiptData> getNextReceiptDataToPull(
      SyncId syncId, int update) async {
    String url = dataUrl(
        syncId.userGuid, syncId.phoneGuid, dataName(RECEIPT_TYPE), update);

    Response response = await get(url);

    SyncReceiptData syncData =
        SyncReceiptData.fromJson(json.decode(response.body));
    return syncData;
  }

  static Future<SyncSearchProductData> getNextSearchProductDataToPull(
      SyncId syncId, int update) async {
        print("getNextSearchProductDataToPull 0");
    String url = dataUrl(
        syncId.userGuid, syncId.phoneGuid, dataName(PRODUCT_TYPE), update);

        print("getNextSearchProductDataToPull 1");
    Response response = await get(url);

        print("getNextSearchProductDataToPull 2");
    SyncSearchProductData syncData =
        SyncSearchProductData.fromJson(json.decode(response.body));

        print("getNextSearchProductDataToPull 3");
    return syncData;
  }

  static Future<SyncSearchStoreData> getNextSearchStoreDataToPull(
      SyncId syncId, int update) async {
    String url = dataUrl(
        syncId.userGuid, syncId.phoneGuid, dataName(STORE_TYPE), update);

    Response response = await get(url);

    SyncSearchStoreData syncData =
        SyncSearchStoreData.fromJson(json.decode(response.body));
    return syncData;
  }

  static String dataUrl(
      String userGuid, String phoneGuid, String dataName, int update) {
    String url = URL +
        'data/' +
        userGuid +
        '/' +
        phoneGuid +
        '/' +
        dataName +
        '/' +
        update.toString();
    return url;
  }

  static String imageName(String path) {
    print(path);

    String prefix = "/Pictures/flutter_test/";
    int i = path.indexOf(prefix);
    if (i < 0) {
      return path;
    } else {
      i = i + prefix.length;
      return path.substring(i);
    }
  }

  static Future<String> imagePath(String name) async {
    Directory extDir = await getApplicationDocumentsDirectory();

    String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);

    return dirPath + "/" + name;
  }

  static Future<void> pullSearchProductData(SyncId syncId) async {
    int noInserted = 0;
    int noUpdated = 0;
    int noNeglected = 0;

    bool act = true;

print("pullSearchProductData 0");
    while (act) {
      int update = await SyncDatabase.getSyncUpdate(PRODUCT_TYPE);

print("pullSearchProductData 1");
      SyncSearchProductData syncData =
          await getNextSearchProductDataToPull(syncId, update);

print("pullSearchProductData 2");
      if (syncData.sync.dataIdentifier == "") {
        act = false;
      } else {
        update = syncData.sync.update;
        syncData.sync.update = 0;
        Sync oldSync = await SyncDatabase.getSync(
            PRODUCT_TYPE, syncData.sync.dataIdentifier);
        if (oldSync.dataIdentifier == "") {
          noInserted = noInserted + 1;
          await SyncDatabase.createImportedSync(syncData.sync);
          if (syncData.sync.action != DELETED) {
            await SyncDatabase.insertSearchProduct(syncData.searchProduct);
          }
        } else {
          if (oldSync.timestamp < syncData.sync.timestamp) {
            noUpdated = noUpdated + 1;
            SyncDatabase.updateImportedSync(syncData.sync);
            await SyncDatabase.deleteSearchProduct(
                syncData.sync.dataIdentifier);
            if (syncData.sync.action != DELETED) {
              await SyncDatabase.insertSearchProduct(syncData.searchProduct);
            }
          } else {
            noNeglected = noNeglected + 1;
          }
        }
        SyncDatabase.updateSyncUpdate(PRODUCT_TYPE, update);
      }
    }

    print('pullSearchProductData: inserted=' +
        noInserted.toString() +
        ' updated=' +
        noUpdated.toString() +
        ' neglected=' +
        noNeglected.toString());
  }

  static Future<void> pullSearchStoreData(SyncId syncId) async {
    int noInserted = 0;
    int noUpdated = 0;
    int noNeglected = 0;

    bool act = true;

    while (act) {
      int update = await SyncDatabase.getSyncUpdate(STORE_TYPE);
      SyncSearchStoreData syncData =
          await getNextSearchStoreDataToPull(syncId, update);
      if (syncData.sync.dataIdentifier == "") {
        act = false;
      } else {
        update = syncData.sync.update;
        syncData.sync.update = 0;
        Sync oldSync = await SyncDatabase.getSync(
            STORE_TYPE, syncData.sync.dataIdentifier);
        if (oldSync.dataIdentifier == "") {
          noInserted = noInserted + 1;
          await SyncDatabase.createImportedSync(syncData.sync);
          if (syncData.sync.action != DELETED) {
            await SyncDatabase.insertSearchStore(syncData.searchStore);
          }
        } else {
          if (oldSync.timestamp < syncData.sync.timestamp) {
            noUpdated = noUpdated + 1;
            SyncDatabase.updateImportedSync(syncData.sync);
            await SyncDatabase.deleteSearchStore(syncData.sync.dataIdentifier);
            if (syncData.sync.action != DELETED) {
              await SyncDatabase.insertSearchStore(syncData.searchStore);
            }
          } else {
            noNeglected = noNeglected + 1;
          }
        }
        SyncDatabase.updateSyncUpdate(STORE_TYPE, update);
      }
    }

    print('pullSearchStoreData: inserted=' +
        noInserted.toString() +
        ' updated=' +
        noUpdated.toString() +
        ' neglected=' +
        noNeglected.toString());
  }
}
