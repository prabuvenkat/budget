import 'dart:async';

import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:budget_onderzoek/database/datamodels/sync_types.dart';
import 'package:sqflite/sqflite.dart';
import 'package:uuid/uuid.dart';

const CREATED = 0;
const UPDATED = 1;
const DELETED = 2;

const GLOBAL_TYPE = 0;
const RECEIPT_TYPE = 1;
const PRODUCT_TYPE = 2;
const STORE_TYPE = 3;

class SyncDatabase {
  static String tableSync = 'tbl_sync';
  static String tableSyncId = 'tbl_sync_id';
  static String tableSyncUpdate = 'tbl_sync_update';

  static Future<void> createTables(Database db) async {
    await db.execute('''
          CREATE TABLE $tableSync (
            data_type int,
            data_identifier TEXT,
            sync_update int,
            sync_timestamp int,
            sync_action int)
          ''');

    await db.execute('''
          CREATE TABLE $tableSyncId (
            userGuid TEXT,
            phoneGuid TEXT,
            phoneName TEXT)
          ''');

    await db.execute('''
          CREATE TABLE $tableSyncUpdate (
            data_type int,
            sync_update int)
          ''');

    await initSyncId();
    await initSyncUpdate(GLOBAL_TYPE);
    await initSyncUpdate(RECEIPT_TYPE);
    await initSyncUpdate(PRODUCT_TYPE);
    await initSyncUpdate(STORE_TYPE);
  }

  static Future initSyncId() async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();
    data['userGuid'] = "";
    data['phoneGuid'] = "";
    data['phoneName'] = Uuid().v1();
    await db.insert(tableSyncId, data);
  }

  static Future updateSyncId(SyncId syncId) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();
    data['userGuid'] = syncId.userGuid;
    data['phoneGuid'] = syncId.phoneGuid;
    data['phoneName'] = syncId.phoneName;
    await db.update(tableSyncId, data);
  }

  static Future<SyncId> getSyncId() async {
    SyncId syncId = new SyncId();
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> results = await db.query(tableSyncId);
    if (results.length >= 1) {
      syncId.userGuid = results[0]['userGuid'];
      syncId.phoneGuid = results[0]['phoneGuid'];
      syncId.phoneName = results[0]['phoneName'];
    }
    return syncId;
  }

//####################################################################

  static Future initSyncUpdate(int dataType) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();
    data['data_type'] = dataType;
    data['sync_update'] = 0;
    await db.insert(tableSyncUpdate, data);
  }

  static Future updateSyncUpdate(int dataType, int update) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();
    data['sync_update'] = update;
    await db.update(tableSyncUpdate, data, 
        where: "data_type = ?", whereArgs: [dataType]);
  }

  static Future<int> getSyncUpdate(int dataType) async {
    int update = 0;
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> results = await db.query(tableSyncUpdate, 
        where: "data_type = ?", whereArgs: [dataType]);
    if (results.length >= 1) {
      update = results[0]['sync_update'];
    }
    return update;
  }

  static Future<int> getAndSetNextUpdate() async {
    int update = await getSyncUpdate(GLOBAL_TYPE);
    update = update + 1;
    await updateSyncUpdate(GLOBAL_TYPE, update);
    return update;
  }

//####################################################################

  static Future createSync(int dataType, String dataIdentifier, int action) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();
    data['data_type'] = dataType;
    data['data_identifier'] = dataIdentifier;
    data['sync_update'] = await getAndSetNextUpdate();
    data['sync_timestamp'] = DateTime.now().millisecondsSinceEpoch;
    data['sync_action'] = action;
    await db.insert(tableSync, data);
  }

  static Future createImportedSync(Sync sync) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();
    data['data_type'] = sync.dataType;
    data['data_identifier'] = sync.dataIdentifier;
    data['sync_update'] = 0;
    data['sync_timestamp'] = sync.timestamp;
    data['sync_action'] = sync.action;
    await db.insert(tableSync, data);
  }

  static Future updateSync(int dataType, String dataIdentifier, int action) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();
    data['sync_update'] = await getAndSetNextUpdate();
    data['sync_timestamp'] = DateTime.now().millisecondsSinceEpoch;
    data['sync_action'] = action;
    await db.update(tableSync, data,
        where: "data_type = ? AND data_identifier = ?", whereArgs: [dataType, dataIdentifier]);
  }

  static Future updateImportedSync(Sync sync) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();
    data['sync_update'] = 0;
    data['sync_timestamp'] = sync.timestamp;
    data['sync_action'] = sync.action;
    await db.update(tableSync, data,
        where: "data_type = ? AND data_identifier = ?", whereArgs: [sync.dataType, sync.dataIdentifier]);
  }

  static Future<Sync> getNextSyncToPush(int localPushed) async {
    Sync sync = new Sync(0, "", 0, 0, 0);
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> results = await db.query(tableSync,
        where: "sync_update > ?",
        whereArgs: [localPushed],
        orderBy: "sync_update");
    
    if (results.length >= 1) {
      sync.dataType = results[0]['data_type'];
      sync.dataIdentifier = results[0]['data_identifier'];
      sync.update = results[0]['sync_update'];
      sync.timestamp = results[0]['sync_timestamp'];
      sync.action = results[0]['sync_action'];
    }
    return sync;
  }

  static Future<Sync> getSync(int dataType, String dataidentifier) async {
    Sync sync = new Sync(0, "", 0, 0, 0);
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> results = await db.query(tableSync,
        where: "data_type = ? AND data_identifier = ?", whereArgs: [dataType, dataidentifier]);
    if (results.length >= 1) {
      sync.dataType = results[0]['data_type'];
      sync.dataIdentifier = results[0]['data_identifier'];
      sync.update = results[0]['sync_update'];
      sync.timestamp = results[0]['sync_timestamp'];
      sync.action = results[0]['sync_action'];
    }
    return sync;
  }

//####################################################################

  static Future insertTransaction(SyncTransaction syncTransaction) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();

    data['transactionID'] = syncTransaction.transactionID;
    data['store'] = syncTransaction.store;
    data['storeType'] = syncTransaction.storeType;
    data['date'] = syncTransaction.date;
    data['discountAmount'] = syncTransaction.discountAmount;
    data['discountPercentage'] = syncTransaction.discountPercentage;
    data['discountText'] = syncTransaction.discountText;
    data['expenseAbroad'] = syncTransaction.expenseAbroad;
    data['expenseOnline'] = syncTransaction.expenseOnline;
    data['totalPrice'] = syncTransaction.totalPrice;
    data['receiptLocation'] = syncTransaction.receiptLocation;
    data['receiptProductType'] = syncTransaction.receiptProductType;

    await db.insert("transactions", data);
  }

  static Future insertProduct(SyncProduct syncProduct) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();

    data['transactionID'] = syncProduct.transactionID;
    data['product'] = syncProduct.product;
    data['productCategory'] = syncProduct.productCategory;
    data['price'] = syncProduct.price;
    data['productCode'] = syncProduct.productCode;
    data['productDate'] = syncProduct.productDate;
    data['productGroupID'] = syncProduct.productGroupID;

    await db.insert("products", data);
  }

  static Future insertSearchProduct(SyncSearchProduct syncSearchProduct) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();

    data['product'] = syncSearchProduct.product;
    data['productCategory'] = syncSearchProduct.productCategory;
    data['productCode'] = syncSearchProduct.productCode;
    data['lastAdded'] = syncSearchProduct.lastAdded;
    data['count'] = syncSearchProduct.count;

    await db.insert("search_suggestions_products", data);
  }

  static Future deleteSearchProduct(String productCode) async {
    Database db = await DatabaseHelper.instance.database;
    await db.delete("search_suggestions_products",
        where: "productCode = ?", whereArgs: [productCode]);
  }

  static Future insertSearchStore(SyncSearchStore syncSearchStore) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();

    data['storeName'] = syncSearchStore.storeName;
    data['storeType'] = syncSearchStore.storeType;
    data['lastAdded'] = syncSearchStore.lastAdded;
    data['count'] = syncSearchStore.count;

    await db.insert("search_suggestions_stores", data);
  }

  static Future deleteSearchStore(String storeName) async {
    Database db = await DatabaseHelper.instance.database;
    await db.delete("search_suggestions_stores",
        where: "storeName = ?", whereArgs: [storeName]);
  }
}
