import 'dart:async';
import 'dart:math';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:budget_onderzoek/database/datamodels/tree.dart';
import 'package:budget_onderzoek/util/date_string.dart';

class ChartData {
  static Tree coicopTree;
  static bool refreshdata = true;

  static double minFilterPrice;
  static double maxFilterPrice;
  static int minFilterDate;
  static int maxFilterDate;
  static int minFoundDate;
  static int maxFoundDate;

  static void reset() {
    refreshdata = true;
  }

  static Future<void> loadChartTables() async {
    if (true || coicopTree == null) {
      refreshdata = false;
      coicopTree = Tree.instance;
      await coicopTree.initCoicop();
      refreshdata = true;
    }
    if (true) {
      refreshdata = false;
      await coicopTree.initProducts(
          minFilterPrice, maxFilterPrice, minFilterDate, maxFilterDate);
    }
  }

  static Map<int, Color> categoryColors = {
    0: ColorPallet.lightGreen,
    1: ColorPallet.orange,
    2: ColorPallet.primaryColor,
    3: ColorPallet.pink,
    4: ColorPallet.darkGreen,
    5: ColorPallet.midblue,
    6: ColorPallet.lightGreen.withOpacity(0.5),
    7: ColorPallet.orange.withOpacity(0.5),
    8: ColorPallet.primaryColor.withOpacity(0.5),
    9: ColorPallet.pink.withOpacity(0.5),
    10: ColorPallet.darkGreen.withOpacity(0.5),
    11: ColorPallet.midblue.withOpacity(0.5),
  };

  static void deterimineFilterValues(
      bool timeChartPage,
      DateTime dayInPeriod,
      String period,
      String minimumPrice,
      String maximumPrice,
      String startDate,
      String endDate) {
    if (minimumPrice == '') {
      minFilterPrice = 0.0;
    } else {
      minFilterPrice = double.parse(minimumPrice);
    }

    if (maximumPrice == '') {
      maxFilterPrice = 1000000000.0;
    } else {
      maxFilterPrice = double.parse(maximumPrice);
    }

    if (startDate == '') {
      minFilterDate = 19000101;
    } else {
      minFilterDate = DateString.dateStringToDateInt(startDate);
    }

    if (endDate == '') {
      maxFilterDate = 22220101;
    } else {
      maxFilterDate = DateString.dateStringToDateInt(endDate);
    }

    if (timeChartPage) {
      String day = DateString.dateTimeToDateString(dayInPeriod);
      String firstDay;
      String lastDay;
      if (period == "week") {
        firstDay = DateString.firstDateOfWeek(day);
        lastDay = DateString.lastDateOfWeek(day);
      } else {
        firstDay = DateString.firstDateOfMonth(day);
        lastDay = DateString.lastDateOfMonth(day);
      }
      minFilterDate =
          max(minFilterDate, DateString.dateStringToDateInt(firstDay));
      maxFilterDate =
          min(maxFilterDate, DateString.dateStringToDateInt(lastDay));
    }
  }

  static dynamic determineFoundDateBoundaries(String coicopId) async {
    minFoundDate = 22220101;
    maxFoundDate = 19000101;
    for (Item i in coicopTree.allItemsDated(coicopId)) {
      int dt = DateString.dateStringToDateInt(i.date);
      minFoundDate = min(minFoundDate, dt);
      maxFoundDate = max(maxFoundDate, dt);
    }
  }

  static Future<void> getFilteredData(
      bool timeChartPage,
      DateTime dayInPeriod,
      String period,
      String minimumPrice,
      String maximumPrice,
      String startDate,
      String endDate) async {
    deterimineFilterValues(timeChartPage, dayInPeriod, period, minimumPrice,
        maximumPrice, startDate, endDate);

    await loadChartTables();
  }

  static Future<List<dynamic>> getData(
      String coicopId,
      DateTime dayInPeriod,
      String period,
      String minimumPrice,
      String maximumPrice,
      String startDate,
      String endDate,
      bool timeChartPage) async {
    List<dynamic> data = [];

    await getFilteredData(false, dayInPeriod, period, minimumPrice,
        maximumPrice, startDate, endDate);
    {
      List<Map<String, dynamic>> data0 = await getAncestors(coicopId);
      while (data0.length > 1) {
        data0.removeAt(0);
      }
      data.add(data0);
      data.add(await getChildren(coicopId));
      data.add(await getDonutChartData(coicopId));
      data.add(coicopTree.nodes[coicopId].value.toString());
    }

    determineFoundDateBoundaries(coicopId);

    await getFilteredData(true, dayInPeriod, period, minimumPrice, maximumPrice,
        startDate, endDate);
    {
      data.add(await getChildren(coicopId));
      data.add(await getBarChartData(coicopId, dayInPeriod, period));
      data.add(minFoundDate);
      data.add(maxFoundDate);
    }

    return data;
  }

  static Map<String, dynamic> insight(
      Item item, String type, double totalSpend, Color color) {
    double allowed = item.value.abs();
    double percentage =
        totalSpend == 0.0 ? 0.0 : (allowed / totalSpend) * 100.0;
    return {
      "code": item.id,
      "name": item.description,
      "parent": item.parentId,
      "spendTotal": double.parse(item.value.toString()),
      "percentage": percentage.toStringAsFixed(0),
      "type": type,
      "color": color
    };
  }

  static Future<List<Map<String, dynamic>>> getAncestors(
      String coicopId) async {
    double totalSpend = coicopTree.nodes[coicopId].value;

    List<Map<String, dynamic>> category = [];

    for (Item item in coicopTree.ancestors(coicopId)) {
      category.add(insight(item, 'ancestor', totalSpend, Color(0xFFEFEFEF)));
    }

    Item item = coicopTree.item(coicopId);
    category.add(insight(item, 'ancestor', totalSpend, Color(0xFF000000)));

    category.sort((a, b) {
      return b['spendTotal'].compareTo(a['spendTotal']);
    });

    return category;
  }

  static Future<List<Map<String, dynamic>>> getChildren(String coicopId) async {
    double totalSpend = coicopTree.nodes[coicopId].value;

    List<Map<String, dynamic>> category = [];

    totalSpend = 0.0;
    for (Item item in coicopTree.children(coicopId)) {
      if (item.value > 0.0) {
        totalSpend = totalSpend + item.value;
      }
    }
    for (Item item in coicopTree.items(coicopId)) {
      if (item.value > 0.0) {
        totalSpend = totalSpend + item.value;
      }
    }

    for (Item item in coicopTree.children(coicopId)) {
      category.add(insight(item, 'child', totalSpend, Color(0xFFEFEFEF)));
    }

    for (Item item in coicopTree.items(coicopId)) {
      category.add(insight(item, 'item', totalSpend, Color(0xFFEFEFEF)));
    }

    category.sort((a, b) {
      return b['spendTotal'].compareTo(a['spendTotal']);
    });

    int colorCounter = 0;
    category.forEach((category) {
      if (category['type'] == 'child' || category['type'] == 'item') {
        category['color'] = categoryColors[colorCounter];
        colorCounter++;
        if (colorCounter == 12) colorCounter = 0;
      }
    });

    return category;
  }

  static dynamic getDonutChartData(String coicopId) async {
    List<Map<String, dynamic>> category1Donut = await getChildren(coicopId);

    category1Donut.removeWhere((category) {
      return category['spendTotal'] == 0;
    });

    if (category1Donut.length < 1) {
      return null;
    }

    if (category1Donut.length == 1) {
      return 1;
    }

    List<ExpenseCategory> data = [];
    category1Donut.forEach((category) {
      int value = int.parse(
          double.parse(category['spendTotal'].toString()).toStringAsFixed(0));
      data.add(ExpenseCategory(category['name'].toString(), value.abs(),
          getChartColor(category['color']), category['code'], double.parse(category['spendTotal'].toString()).toStringAsFixed(2)));
    });

    return [
      charts.Series<ExpenseCategory, String>(
        id: 'Category',
        domainFn: (ExpenseCategory category, _) => category.categoryName,
        measureFn: (ExpenseCategory category, _) => category.moneySpent,
        data: data,
        colorFn: (ExpenseCategory category, _) => category.color,
        labelAccessorFn: (ExpenseCategory category, _) =>
            '€${category.label}',
        outsideLabelStyleAccessorFn: (ExpenseCategory category, _) =>
            charts.TextStyleSpec(
                fontFamily: 'Source Sans Pro Bold',
                fontSize: (16).floor(), // size in Pts.
                color: getChartColor(ColorPallet.darkTextColor)),
      )
    ];
  }

  static dynamic getBarChartData(
      String coicopId, DateTime dayInPeriod, String period) async {
    List<Map<String, dynamic>> childColors = await getChildren(coicopId);

    String day = DateString.dateTimeToDateString(dayInPeriod);
    String firstDay;
    String lastDay;
    if (period == "week") {
      firstDay = DateString.firstDateOfWeek(day);
      lastDay = DateString.lastDateOfWeek(day);
    } else {
      firstDay = DateString.firstDateOfMonth(day);
      lastDay = DateString.lastDateOfMonth(day);
    }
    DateTime minDate = DateTime.parse(firstDay);
    DateTime maxDate = DateTime.parse(lastDay);

    Map<String, Map<String, double>> allItemsDated = new Map();
    for (Item i in coicopTree.allItemsDated(coicopId)) {
      int dt = DateString.dateStringToDateInt(i.date);
    }

    bool itemsFound = false;
    allItemsDated = new Map();
    for (Item i in coicopTree.periodItemsDated(coicopId, firstDay, lastDay)) {
      itemsFound = true;
      if (!allItemsDated.containsKey(i.description)) {
        allItemsDated[i.description] = {i.date: i.value};
      } else {
        if (!allItemsDated[i.description].containsKey(i.date)) {
          allItemsDated[i.description][i.date] = i.value;
        } else {
          allItemsDated[i.description][i.date] += i.value;
        }
      }
    }

    if (!itemsFound) {
      allItemsDated[''] = new Map();
    }

    Map<String, Map<String, double>> allDates = new Map();
    for (String id in allItemsDated.keys) {
      allDates[id] = new Map();
      DateTime dt = minDate;
      while (dt.compareTo(maxDate) <= 0) {
        String dd = DateString.dateTimeToDateString(dt);
        allDates[id][dd] = 0.0;
        dt = dt.add(Duration(days: 1));
      }
      for (String dd in allItemsDated[id].keys) {
        allDates[id][dd] = allItemsDated[id][dd];
      }
    }

    Map<String, List<ExpenseDate>> barData = new Map();
    for (String id in allDates.keys) {
      barData[id] = [];
      for (String dd in allDates[id].keys) {
        barData[id].add(ExpenseDate(dd, allDates[id][dd]));
      }
    }

    List<charts.Series<ExpenseDate, String>> barChartData = [];
    for (String id in barData.keys) {
      Color barColor = Colors.blue;
      for (Map<String, dynamic> col in childColors) {
        if (col['name'].toString() == id) {
          barColor = col['color'];
        }
      }
      barChartData.add(new charts.Series<ExpenseDate, String>(
        id: id,
        domainFn: (ExpenseDate sales, _) => period == "week"
            ? DateString.barChartWeekday(sales.date)
            : DateString.barChartMonthday(sales.date),
        measureFn: (ExpenseDate sales, _) => sales.expense,
        data: barData[id],
        colorFn: (ExpenseDate sales, _) => getChartColor(barColor),
      ));
    }

    return barChartData;
  }

  static charts.Color getChartColor(Color color) {
    return charts.Color(
        r: color.red, g: color.green, b: color.blue, a: color.alpha);
  }
}

class ExpenseCategory {
  ExpenseCategory(
      this.categoryName, this.moneySpent, this.color, this.snapshotCode, this.label);

  final String categoryName;
  final charts.Color color;
  final String label;
  final int moneySpent;
  final snapshotCode;
}

class ExpenseDate {
  ExpenseDate(this.date, this.expense);

  final String date;
  final double expense;
}
