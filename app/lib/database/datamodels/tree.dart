import 'dart:async';
import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:sqflite/sqflite.dart';
import 'package:budget_onderzoek/util/date_string.dart';

class Item {
  Item(this.id, this.description, this.value, this.parentId, this.date);

  String date;
  String description;
  String id;
  String parentId;
  double value;

  int get dateInt {
    return DateString.dateStringToDateInt(date);
  }
}

class Node {
  Node(this.id, this.description, this.type);

  List<String> children = [];
  String description;
  String id;
  List<Item> items = [];
  int type;
  double value = 0.0;
}

class Tree {
  Tree._() {}

  static final Tree instance = Tree._();

  String marker = ' #_# ';
  int maxProductDate;
  int minProductDate;
  Map<String, Node> nodes = {};
  String selectedId = '.';
  String separator = '.';

  String get rootId {
    return separator;
  }

  Node get selectedNode {
    return nodes[selectedId];
  }

  Future<void> initCoicop() async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> tblCoicop =
        await db.query('tblCoicop' + '_' + LanguageSetting.tablePreference);

    nodes.clear();
    addNode(rootId, Translations.textStatic('mainCategories', 'Overview'));
    tblCoicop.forEach((coicop) {
      addNode(coicop['code'].toString(), coicop['coicop'].toString());
    });
  }

  Future<void> initProducts(double minFilterPrice, double maxFilterPrice,
      int minFilterDate, int maxFilterDate) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> tblProducts = await db.query('products');

    deleteAllItems();
    minProductDate = 25000101;
    maxProductDate = 19000101;

    int nn = 0;
    tblProducts.forEach((product) {
      int prodDate = int.parse(product['productDate'].toString());
      double price = double.parse(product['price'].toString());

      nn = nn + 1;
      if (minFilterPrice <= price &&
          price <= maxFilterPrice &&
          minFilterDate <= prodDate &&
          prodDate <= maxFilterDate &&
          price > 0.0) {
        addItem(
            nn,
            product['productCode'].toString(),
            product['product'].toString(),
            double.parse(product['price'].toString()),
            DateString.dateIntToDateString(prodDate));

        minProductDate = minProductDate < prodDate ? minProductDate : prodDate;
        maxProductDate = maxProductDate > prodDate ? maxProductDate : prodDate;
      }
    });

    calculateAllValues();
  }

  void calculateAllValues() {
    _calculateValues(rootId);
  }

  void _calculateValues(String id) {
    double sum = 0;
    Node node = nodes[id];
    for (String child in node.children) {
      _calculateValues(child);
      sum += nodes[child].value;
    }
    for (Item item in node.items) {
      sum += item.value;
    }
    node.value = sum;
  }

  void deleteAllItems() {
    _deleteItems(rootId);
    _deleteItemNodes();
  }

  void _deleteItems(String id) {
    Node node = nodes[id];
    for (String child in node.children) {
      _deleteItems(child);
    }
    node.items.clear();
    node.value = 0.0;
  }

  void _deleteItemNodes() {
    nodes.forEach((k,v) => v.children.removeWhere((c) => nodes[c].type == 2));
    nodes.removeWhere((k,v) => v.type == 2);
  }

  String parentId(String id) {
    if (id == rootId) {
      return id;
    } else if (!id.contains(separator)) {
      return rootId;
    }
    return id.substring(0, id.lastIndexOf(separator));
  }

  void addNode(String id, String description) {
    if (!nodes.keys.contains(id)) {
      nodes[id] = new Node(id, description, 1);
      _addNodeToParent(id);
    } else {
      nodes[id].description = description;
    }
  }

  void _addParentNode(String id) {
    if (!nodes.keys.contains(id)) {
      nodes[id] = new Node(id, '?', 1);
      _addNodeToParent(id);
    }
  }

  void _addNodeToParent(String id) {
    if (id != rootId) {
      String parent = parentId(id);
      _addParentNode(parent);
      if (!nodes[parent].children.contains(id)) {
        nodes[parent].children.add(id);
      }
    }
  }

  void addItem(
      int nn, String id, String description, double value, String date) {
    _addParentNode(id);
    String itemId = id + separator + '_' + nn.toString();
    String itemDescription = description + marker + nn.toString();
    nodes[itemId] = new Node(itemId, itemDescription, 2);
    nodes[id].children.add(itemId);
    nodes[itemId].items.add(new Item(itemId, itemDescription, value, id, date));
  }

  void addItemOKE(String id, String description, double value, String date) {
    _addParentNode(id);
    nodes[id].items.add(new Item(id, description, value, parentId(id), date));
  }

  Item item(String id) {
    if (!nodes.keys.contains(id)) {
      return new Item(
          id, 'Id ' ' + id + ' ' does not exist!', 0.0, parentId(id), '');
    }
    return new Item(
        id, nodes[id].description, nodes[id].value, parentId(id), '');
  }

  List<Item> items(String id) {
    List<Item> result = [];

    if (!nodes.keys.contains(id)) {
      result.add(new Item(
          id, 'Id ' ' + id + ' ' does not exist!', 0.0, parentId(id), ''));
      return result;
    }

    Map<String, Item> sumItems = {};
    for (Item i in nodes[id].items) {
      if (!sumItems.keys.contains(i.description)) {
        sumItems[i.description] =
            Item(i.id, i.description, i.value, i.parentId, i.date);
      } else {
        sumItems[i.description].value += i.value;
      }
    }
    for (Item i in sumItems.values) {
      result.add(i);
    }

    return result;
  }

  List<Item> allItemsDated(String id) {
    List<Item> result = [];

    for (Item i in nodes[id].items) {
      result.add(Item(id, i.description, i.value, id, i.date));
    }

    for (String child in nodes[id].children) {
      for (Item i in allItemsDated(child)) {
        result
            .add(Item(child, nodes[child].description, i.value, child, i.date));
      }
    }

    return result;
  }

  List<Item> periodItemsDated(String id, String fromDate, String toDate) {
    List<Item> result = [];
    for (Item i in allItemsDated(id)) {
      if (fromDate.compareTo(i.date) <= 0 && toDate.compareTo(i.date) >= 0) {
        result.add(i);
      }
    }
    return result;
  }

  List<Item> children(String id) {
    List<Item> result = [];

    if (!nodes.keys.contains(id)) {
      result.add(new Item(
          id, 'Id ' ' + id + ' ' does not exist!', 0.0, parentId(id), ''));
      return result;
    }

    for (String child in nodes[id].children) {
      if (item(child).value >= 0) {
        result.add(item(child));
      }
    }

    return result;
  }

  List<Item> ancestorsInclusive(String id) {
    List<Item> result = [];

    if (!nodes.keys.contains(id)) {
      result.add(new Item(
          id, 'Id ' ' + id + ' ' does not exist!', 0, parentId(id), ''));
      return result;
    }

    if (id != rootId) {
      for (Item i in ancestorsInclusive(parentId(id))) {
        result.add(i);
      }
    }

    result.add(item(id));

    return result;
  }

  List<Item> ancestors(String id) {
    List<Item> result = ancestorsInclusive(id);
    result.removeLast();
    return result;
  }
}
