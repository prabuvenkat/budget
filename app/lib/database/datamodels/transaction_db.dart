import 'dart:async';

import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:budget_onderzoek/database/datamodels/charts_db.dart';
import 'package:budget_onderzoek/scoped_models/expenselist_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/fliter_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/util/algorithms/string_matching_products.dart';
import 'package:budget_onderzoek/util/algorithms/string_matching_shops.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';

class TransactionDatabase {
  static String tableProduct = 'products';
  static String tableProductBackup = 'product_backup';
  static String tableTransaction = 'transactions';
  static String tableTransactionBackup = 'transactions_backup';
  static String tableUserProgress = 'user_progress';

  String date = "";
  String discountAmount = "";
  String discountPercentage = "";
  String discountText = "";
  String expenseAbroad = "";
  String expenseOnline = "";
  String price = "";
  String product = "";
  String productCategory = "";
  String productCode = "";
  String productDate = "";
  String productGroupID = "";
  String receiptLocation = "";
  String receiptProductType = "";
  String store = "";
  String storeType = "";
  String totalPrice = "";
  String transactionID = "";

  static Future<void> createTables(Database db) async {
    await db.execute('''
          CREATE TABLE $tableTransaction (
            transactionID TEXT,
            store TEXT,
            storeType TEXT,
            date int,
            discountAmount TEXT,
            discountPercentage TEXT,
            expenseAbroad TEXT,
            expenseOnline TEXT,
            totalPrice float,
            discountText TEXT,
            receiptLocation TEXT,
            receiptProductType TEXT)
          ''');

    await db.execute('''
          CREATE TABLE $tableTransactionBackup (
            transactionID TEXT,
            store TEXT,
            storeType TEXT,
            date int,
            discountAmount TEXT,
            discountPercentage TEXT,
            expenseAbroad TEXT,
            expenseOnline TEXT,
            totalPrice float,
            discountText TEXT,
            receiptLocation TEXT,
            receiptProductType TEXT)
          ''');

    await db.execute('''
          CREATE TABLE $tableProduct (
            transactionID TEXT,
            product TEXT,
            productCategory TEXT,
            price float,
            productCode TEXT,
            productDate TEXT,
            productGroupID TEXT)
          ''');

    await db.execute('''
          CREATE TABLE $tableProductBackup (
            transactionID TEXT,
            product TEXT,
            productCategory TEXT,
            price float,
            productCode TEXT,
            productDate TEXT,
            productGroupID TEXT)
          ''');

    await db.execute('''
          CREATE TABLE $tableUserProgress (
            daysOfExperiment int,
            daysCompleted int,
            daysMissing int)
          ''');
  }

  static void insertTransaction(TransactionDatabase trx) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();

    data['transactionID'] = trx.transactionID;
    data['store'] = trx.store;
    data['storeType'] = trx.storeType;
    data['date'] = trx.date.replaceAll('-', '');
    data['discountAmount'] = trx.discountAmount;
    data['discountPercentage'] = trx.discountPercentage;
    data['discountText'] = trx.discountText;
    data['expenseAbroad'] = trx.expenseAbroad;
    data['expenseOnline'] = trx.expenseOnline;
    data['totalPrice'] = trx.totalPrice;
    data['receiptLocation'] = trx.receiptLocation;
    data['receiptProductType'] = trx.receiptProductType;

    await db.insert(tableTransaction, data);
    ChartData.reset();
  }

  static void insertProduct(TransactionDatabase trx) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();

    data['transactionID'] = trx.transactionID;
    data['product'] = trx.product;
    data['productCategory'] = trx.productCategory;
    data['price'] = trx.price;
    data['productCode'] = trx.productCode;
    data['productDate'] = trx.productDate.replaceAll('-', '');
    data['productGroupID'] = trx.productGroupID;

    await db.insert(tableProduct, data);
  }

  static Future<List<Map<String, dynamic>>> queryAllTransactions(
      BuildContext context) async {
    var filtermodel = FilterModel.of(context);
    Database db = await DatabaseHelper.instance.database;
    String innerjoin = "";
    String groupby = "";
    List<String> whereClauses = [];

    if (filtermodel.startDate != "" || filtermodel.endDate != "") {
      Map<String, dynamic> dbValues = await getFirstAndLastDate();
      String startDateFilter = filtermodel.startDate != ""
          ? filtermodel.startDate
          : dbValues['lowest'];
      String endDateFilter =
          filtermodel.endDate != "" ? filtermodel.endDate : dbValues['highest'];
      startDateFilter = startDateFilter.replaceAll("-", "");
      endDateFilter = endDateFilter.replaceAll("-", "");
      whereClauses.add(" date >= $startDateFilter AND date <= $endDateFilter ");
    }

    String searchWord = ExpenseListModel.of(context).searchWord;
    if (searchWord != null) {
      if (searchWord != "") {
        innerjoin =
            "INNER JOIN $tableProduct ON $tableTransaction.transactionID = $tableProduct.transactionID";
        groupby = "GROUP BY $tableTransaction.transactionID";
        whereClauses.add(
            "(product LIKE '%$searchWord%' OR store LIKE '%$searchWord%' OR storeType LIKE '%$searchWord%')");
      }
    }

    if (filtermodel.minimumPrice != "") {
      whereClauses
          .add("CAST(totalPrice as INT) >= ${filtermodel.minimumPrice}");
    }
    if (filtermodel.maximumPrice != "") {
      whereClauses
          .add("CAST(totalPrice as INT) <= ${filtermodel.maximumPrice}");
    }

    String query = "SELECT  * FROM $tableTransaction $innerjoin";
    String joiner = " WHERE ";
    for (String where in whereClauses) {
      query += joiner + where;
      joiner = " AND ";
    }
    query = "$query $groupby ORDER BY date";
    return await db.rawQuery(query);
  }

  static Future<List<Map<String, dynamic>>> queryTransaction(
      String transactionID) async {
    Database db = await DatabaseHelper.instance.database;
    return await db.query(
      tableTransaction,
      where: "transactionID = ?",
      whereArgs: [transactionID],
    );
  }

  static Future<List<Map<String, dynamic>>> queryTransactionProducts(
      String transactionID) async {
    Database db = await DatabaseHelper.instance.database;
    return await db.query(
      tableProduct,
      where: "transactionID = ?",
      whereArgs: [transactionID],
    );
  }

  static Future getTransactionInfo(
      String transactionID, NewTransactionModel newTransaction) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> resultStore = await db.query(tableTransaction,
        where: "transactionID = ?", whereArgs: [transactionID]);
    newTransaction.store = resultStore[0]['store'];
    newTransaction.storeType = resultStore[0]['storeType'];
    newTransaction.date = resultStore[0]['date'].toString();
    newTransaction.discountText = resultStore[0]['discountText'];
    newTransaction.receiptLocation = resultStore[0]['receiptLocation'];
    newTransaction.receiptPrice = resultStore[0]['totalPrice'].toString();
    newTransaction.discountAmount =
        double.parse(resultStore[0]['discountAmount'].toString());
    newTransaction.receiptProductTypeString =
        resultStore[0]['receiptProductType'];

    List<Map<String, dynamic>> resultProducts = await db.query(tableProduct,
        where: "transactionID = ?", whereArgs: [transactionID]);

    resultProducts.forEach((productInfo) {
      // print(productInfo);
      newTransaction.product = productInfo['product'].toString();
      newTransaction.productCategory =
          productInfo['productCategory'].toString();
      newTransaction.price = productInfo['price'].toString();
      if (newTransaction.receiptLocation == "") {
        newTransaction.addNewProduct(oldProductGroupId: productInfo['productGroupID']);
      }
    });

    newTransaction.discountPercentage =
        double.parse(resultStore[0]['discountPercentage']);
    newTransaction.abroadExpense =
        resultStore[0]['expenseAbroad'].toLowerCase() == 'true';
    newTransaction.onlineExpense =
        resultStore[0]['expenseOnline'].toLowerCase() == 'true';
    newTransaction.totalPrice =
        double.parse(resultStore[0]['totalPrice'].toString());
  }

  static Future createTransactionBackup(String transactionID) async {
    Database db = await DatabaseHelper.instance.database;

    List<Map<String, dynamic>> resultTransaction = await db.rawQuery(
        "SELECT * FROM $tableTransaction WHERE transactionID = '$transactionID'");
    List<Map<String, dynamic>> resultProducts = await db.rawQuery(
        "SELECT * FROM $tableProduct WHERE transactionID = '$transactionID'");

    for (Map<String, dynamic> row in resultTransaction) {
      await db.insert(tableTransactionBackup, row);
    }

    for (Map<String, dynamic> row in resultProducts) {
      await db.insert(tableProductBackup, row);
    }
  }

  static Future restoreBackUp(String transactionID) async {
    Database db = await DatabaseHelper.instance.database;

    List<Map<String, dynamic>> resultTransaction = await db.rawQuery(
        "SELECT * FROM $tableTransactionBackup  WHERE transactionID = '$transactionID'");
    List<Map<String, dynamic>> resultProducts = await db.rawQuery(
        "SELECT * FROM $tableProductBackup  WHERE transactionID = '$transactionID'");

    for (Map<String, dynamic> row in resultTransaction) {
      await db.insert(tableTransaction, row);
    }

    for (Map<String, dynamic> row in resultProducts) {
      await db.insert(tableProduct, row);
    }

    await db.delete(tableTransactionBackup,
        where: "transactionID = ?", whereArgs: [transactionID]);

    await db.delete(tableProductBackup,
        where: "transactionID = ?", whereArgs: [transactionID]);
  }

  static Future deleteBackUp() async {
    Database db = await DatabaseHelper.instance.database;
    await db.delete(tableProductBackup);
    await db.delete(tableTransactionBackup);
  }

  static Future deleteTransaction(String transactionID) async {
    Database db = await DatabaseHelper.instance.database;
    await db.delete(tableProduct,
        where: "transactionID = ?", whereArgs: [transactionID]);
    await db.delete(tableTransaction,
        where: "transactionID = ?", whereArgs: [transactionID]);
    ChartData.reset();
  }

  static Future deleteTransactionWithBackup(String transactionID) async {
    await createTransactionBackup(transactionID);
    await deleteTransaction(transactionID);
  }

  static Future<Map<String, dynamic>> getFirstAndLastDate() async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> results =
        await db.query(tableTransaction, orderBy: "date", columns: ["date"]);
    String first;
    String last;

    if (results.length >= 1) {
      first = results[0]['date'].toString();
      last = results[results.length - 1]['date'].toString();
    } else {
      first = DateTime.now().toString();
      last = (DateTime.now()).add(Duration(days: 30)).toString();

      first = DateFormat('yyyy-MM-dd').format(DateTime.parse(first));
      last = DateFormat('yyyy-MM-dd').format(DateTime.parse(last));
    }
    return {"first": first, "last": last};
  }

  static Future<Map<String, dynamic>> getLowestAndHighestValue() async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> results = await db.query(tableTransaction,
        orderBy: "totalPrice", columns: ["totalPrice"]);

    String lowest;
    String highest;
    if (results.length >= 1) {
      lowest = results[0]['totalPrice'].toString();
      lowest = double.parse(lowest).toStringAsFixed(0);
      highest = results[results.length - 1]['totalPrice'].toString();
      highest = double.parse(highest).toStringAsFixed(0);
    } else {
      lowest = "0";
      highest = "0";
    }
    return {"lowest": lowest, "highest": highest};
  }

  static Future<String> getProductCode(String coicop) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> code = await db.query(
        "tblProduct" + "_" + LanguageSetting.tablePreference,
        where: "Coicop = ?",
        whereArgs: [coicop],
        columns: ["code"]);

    // print(code);

    if (code != null) {
      if (code.length != 0) {
        return code[0]['code'];
      } else {
        //Coicop code is not seen in product table, therefore look it up in the coicop table:
        //mogelijk gaat hier iets verkeerd icm taal veranderen
        List<Map<String, dynamic>> codeCoicop = await db.query(
            "tblCoicop" + "_" + LanguageSetting.tablePreference,
            where: "Coicop = ?",
            whereArgs: [coicop],
            columns: ["code"]);
        return codeCoicop[0]['code'];
      }
    } else {
      return "";
    }
  }

  /*If a user adds a product that hasn't beens een before, this method will add it to the list of products */
  static void addNewProductOption(String product, String coicop) async {
    Database db = await DatabaseHelper.instance.database;

    List<Map<String, dynamic>> doesItExist = await db.query(
        "tblProduct" + "_" + LanguageSetting.tablePreference,
        where: "product = ? AND coicop = ?",
        whereArgs: [product.toLowerCase(), coicop]);

    if (doesItExist.length == 0) {
      Map<String, dynamic> data = Map<String, dynamic>();
      data['code'] = '12.8.0.0';
      data['frequency'] = '0';
      data['product'] = product.toLowerCase();
      data['coicop'] = coicop;
      await db.insert("tblProduct" + "_" + LanguageSetting.tablePreference, data);
      StringMatchingProducts.resetProductTable();
    }
  }

  /*If a user adds a product that hasn't been seen before, this method will add it to the list of products */
  static void addNewShopOption(String shop, String shoptype) async {
    Database db = await DatabaseHelper.instance.database;

    List<Map<String, dynamic>> doesItExist = await db.rawQuery(
        "SELECT * FROM tblShop_${LanguageSetting.tablePreference} WHERE shop LIKE '$shop' AND shoptype LIKE '$shoptype'");

    if (doesItExist.length == 0) {
      Map<String, dynamic> data = Map<String, dynamic>();
      data['shop'] = shop;
      data['shoptype'] = shoptype;
      await db.insert("tblShop" + "_" + LanguageSetting.tablePreference, data);
      StringMatchingShops.resetShopTable();
    }
  }

  static Future<List<Map<String, dynamic>>> queryDailyTransactions(
      BuildContext context, String year, String month, String day) async {
    month = month.length == 1 ? "0$month" : month;
    day = day.length == 1 ? "0$day" : day;
    Database db = await DatabaseHelper.instance.database;
    String query =
        "SELECT  * FROM $tableTransaction WHERE date = $year$month$day";
    var results = await db.rawQuery(query);
    return results;
  }

  static Future<String> lookupMainCategoryReceiptType(
      String lookupValue) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> categoryName = await db.rawQuery(
        "SELECT ${LanguageSetting.key.toString()} FROM tblTranslations WHERE code = '$lookupValue'");
    List<Map<String, dynamic>> receiptName = await db.rawQuery(
        "SELECT ${LanguageSetting.key.toString()} FROM tblTranslations WHERE code = '00'");

    return ('${receiptName[0][LanguageSetting.key]} ${categoryName[0][LanguageSetting.key]}');
  }
}
