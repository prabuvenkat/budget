import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/translations/international.dart';
import 'package:intl/intl.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class DateString {
  static String dateTimeToDateString(DateTime dt) {
    return dt.toString().substring(0, 10);
  }

  static int dateTimeToDateInt(DateTime dt) {
    return dateStringToDateInt(dateTimeToDateString(dt));
  }

  static int dateStringToDateInt(String date) {
    return int.parse(date.replaceAll('-', ''));
  }

  static String dateIntToDateString(int date) {
    String dd = date.toString();
    String result = dd.substring(0, 4) +
        '-' +
        dd.substring(4, 6) +
        '-' +
        dd.substring(6, 8);
    return result;
  }

  static DateTime dateStringToDateTime(String date) {
    int y = int.parse(date.substring(0, 4));
    int m = int.parse(date.substring(5, 7));
    int d = int.parse(date.substring(8, 10));
    return DateTime(y, m, d);
  }

  static DateTime dateIntToDateTime(int date) {
    return dateStringToDateTime(dateIntToDateString(date));
  }

  static String dayOfWeek(int weekday) {
    String result = '';
    switch (weekday) {
      case 1:
        result = Translations.textStatic('monday', '?');
        break;
      case 2:
        result = Translations.textStatic('tuesday', '?');
        break;
      case 3:
        result = Translations.textStatic('wednesday', '?');
        break;
      case 4:
        result = Translations.textStatic('thursday', '?');
        break;
      case 5:
        result = Translations.textStatic('friday', '?');
        break;
      case 6:
        result = Translations.textStatic('saturday', '?');
        break;
      case 7:
        result = Translations.textStatic('sunday', '?');
        break;
      default:
        result = Translations.textStatic('monday', '?');
    }
    return result;
  }

  static String firstDateOfMonth(String date) {
    DateTime dt;
    dt = DateTime.parse(date);
    dt = dt.add(Duration(days: 1 - dt.day));
    return dateTimeToDateString(dt);
  }

  static String lastDateOfMonth(String date) {
    DateTime dt;
    dt = DateTime.parse(firstDateOfMonth(date));
    dt = dt.add(Duration(days: 35));
    dt = DateTime.parse(firstDateOfMonth(dateTimeToDateString(dt)));
    dt = dt.add(Duration(hours: -1));
    return dateTimeToDateString(dt);
  }

  static String firstDateOfWeek(String date) {
    DateTime dt;
    dt = DateTime.parse(date);
    dt = dt.add(Duration(days: 1 - dt.weekday));
    return dateTimeToDateString(dt);
  }

  static String lastDateOfWeek(String date) {
    DateTime dt;
    dt = DateTime.parse(date);
    dt = dt.add(Duration(days: 7 - dt.weekday));
    return dateTimeToDateString(dt);
  }

  static String monthString(DateTime dt) {
    String result = DateFormat.MMMM(International.languageFromId(LanguageSetting.key).localeKey)
        .format(DateTime(dt.year, dt.month, 1));
    return (result[0].toUpperCase() + result.substring(1));
  }

  static bool nextPeriodExists(DateTime nextDate, int absMinDate,
      int absMaxDate, String period, int swipe) {
    String dd = dateTimeToDateString(nextDate);
    DateTime firstDay;
    DateTime lastDay;

    if (period == "week") {
      firstDay = DateTime.parse(firstDateOfWeek(dd));
      lastDay = DateTime.parse(lastDateOfWeek(dd));
    } else {
      firstDay = DateTime.parse(firstDateOfMonth(dd));
      lastDay = DateTime.parse(lastDateOfMonth(dd));
    }

    if (absMinDate > absMaxDate) {
      return false;
    }
    if (swipe == -1 &&
        absMinDate > dateStringToDateInt(dateTimeToDateString(lastDay))) {
      return false;
    }
    if (swipe == 1 &&
        absMaxDate < dateStringToDateInt(dateTimeToDateString(firstDay))) {
      return false;
    }
    return true;
  }

  static String barChartTitle(DateTime dayInPeriod, String period) {
    String result;

    String dd = dateTimeToDateString(dayInPeriod);
    DateTime firstDay;
    DateTime lastDay;

    if (period == "week") {
      firstDay = DateTime.parse(firstDateOfWeek(dd));
      lastDay = DateTime.parse(lastDateOfWeek(dd));
    } else {
      firstDay = DateTime.parse(firstDateOfMonth(dd));
      lastDay = DateTime.parse(lastDateOfMonth(dd));
    }

    if (period == "week") {
      result = firstDay.day.toString() +
          '/' +
          firstDay.month.toString() +
          ' - ' +
          lastDay.day.toString() +
          '/' +
          lastDay.month.toString();
    } else {
      result = monthString(lastDay) + ' ' + lastDay.year.toString();
    }

    return result;
  }

  static String barChartWeekday(String date) {
    DateTime dt = DateTime.parse(date);
    return DateString.dayOfWeek(dt.weekday) + '-' + dt.day.toString();
  }

  static String barChartMonthday(String date) {
    String result;
    int dd = DateTime.parse(date).day;
    String d = date.substring(9, 10);
    if (dd <= 10) {
      result = d;
    } else if (dd <= 20) {
      result = '' + d + ' ';
    } else if (dd <= 30) {
      result = ' ' + d + '';
    } else if (dd <= 40) {
      result = ' ' + d + ' ';
    }
    return dd.toString();
  }

  static List<charts.TickSpec<String>> barChartTickSpec(
      DateTime dayInPeriod, String period) {
    List<charts.TickSpec<String>> result = [];

    Map<String, String> labels = barChartLabels(dayInPeriod, period);

    for (String lab in labels.keys) {
      result.add(charts.TickSpec(lab, label: labels[lab]));
    }

    return result;
  }

  static Map<String, String> barChartLabels(
      DateTime dayInPeriod, String period) {
    Map<String, String> labels = {};

    String dd = dateTimeToDateString(dayInPeriod);
    DateTime firstDay;
    DateTime lastDay;

    if (period == "week") {
      firstDay = DateTime.parse(firstDateOfWeek(dd));
      lastDay = DateTime.parse(lastDateOfWeek(dd));
    } else {
      firstDay = DateTime.parse(firstDateOfMonth(dd));
      lastDay = DateTime.parse(lastDateOfMonth(dd));
    }

    DateTime dt = firstDay;
    int tick = 1;
    while (dt.compareTo(lastDay) <= 0) {
      if (period == "week") {
        String lab = barChartWeekday(dateTimeToDateString(dt));
        labels[lab] =
            barChartWeekday(dateTimeToDateString(dt)); //dayOfWeek(tick);
      } else {
        String lab = barChartMonthday(dateTimeToDateString(dt));
        labels[lab] = tick == 1 || tick % 5 == 0 ? tick.toString() : '';
      }
      tick++;
      dt = dt.add(Duration(days: 1));
    }

    return labels;
  }
}
