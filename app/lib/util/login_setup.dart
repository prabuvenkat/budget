import 'package:budget_onderzoek/database/datamodels/user_progress_db.dart';
import 'package:budget_onderzoek/main.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import 'package:budget_onderzoek/database/datamodels/sync.dart';
import 'package:budget_onderzoek/database/datamodels/sync_types.dart';
import 'package:budget_onderzoek/database/datamodels/sync_db.dart';

class Login {
  static void validateInput(BuildContext context, String password) async {
    String usr = password.substring(0, password.indexOf("###***###"));
    String pwd = password.substring(password.indexOf("###***###") + 9);
    print("validateInput: " + usr + " - " + pwd);
    SyncId syncId = await SyncDatabase.getSyncId();
    if (Synchronise.backendIsActive()) {
      try {
        SyncUser syncUser = await Synchronise.registerUser(usr, pwd);
        if (syncUser.id != 0) {
          syncId.userGuid = syncUser.guid;
          await SyncDatabase.updateSyncId(syncId);
        }

        if (syncId.userGuid != "") {
          SyncPhone syncPhone = await Synchronise.registerPhone(
              syncId.userGuid, syncId.phoneName);
          if (syncPhone.id != 0) {
            syncId.phoneGuid = syncPhone.guid;
            await SyncDatabase.updateSyncId(syncId);
          }
        }
      } catch (e) {
        Toast.show(e.toString(), context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    }

    if ((password.toLowerCase() == "hbs###***###2020") ||
        (syncId.userGuid != "" && syncId.phoneGuid != "") || true) {
      UserProgressDatabase.initializeData();
      saveLanguagePreference();
      saveTablePreference();
      loadAppPage(context);
      changeStatusBarColor();
    } else {
      if (usr.toLowerCase() != "hbs") {
        Toast.show(translations.text ('incorrectUserName'), context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      } else {
        Toast.show(translations.text ('incorrectPassword'), context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
    }
  }

  static saveLanguagePreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('languagePreference', LanguageSetting.key);
  }

  static saveTablePreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('tablePreference', LanguageSetting.tablePreference);
  }

  static void changeStatusBarColor() {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
          statusBarColor: ColorPallet.primaryColor,
          systemNavigationBarColor: Colors.black,
          systemNavigationBarIconBrightness: Brightness.light),
    );
  }

  static void loadAppPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PageNavigator(),
      ),
    );
  }
}
