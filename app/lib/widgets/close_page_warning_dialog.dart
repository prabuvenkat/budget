
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:budget_onderzoek/translations/translations.dart';


Translations translations;

class ClosingPageWarning extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Manual_Entry');
    return AlertDialog(
      titlePadding: EdgeInsets.all(0),
      title: Container(
        color: ColorPallet.pink,
        padding: EdgeInsets.all(20),
        child: Row(
          children: <Widget>[
            Text(
              translations.text ('warning'),
              style: TextStyle(
                  fontSize: 20 * f,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
            ),
          ],
        ),
      ),
      content: Text(
          translations.text ('removalWarningText'),
          style: TextStyle(
              fontSize: 17 * f,
              fontWeight: FontWeight.w400,
              color: ColorPallet.darkTextColor)),
      actions: <Widget>[
        FlatButton(
          child: Text(translations.text ('delete'), style: TextStyle(color: ColorPallet.pink)),
          onPressed: () {
            SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
              statusBarColor: ColorPallet.primaryColor,
            ));

            Navigator.of(context).pop();
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          child: Text(translations.text ('cancel'), style: TextStyle(color: ColorPallet.pink)),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}