import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/animated_focus_light.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/content_target.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/target_focus.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/tutorial_coach_mark.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:budget_onderzoek/translations/translations.dart';




List<TargetFocus> targets = List();

TextStyle titleStyle = TextStyle(
  fontWeight: FontWeight.w700,
  color: Colors.white,
  fontSize: 22.0 * f,
  height: 1.4,
);
TextStyle bodyStyle = TextStyle(
  fontWeight: FontWeight.w400,
  color: Colors.white,
  fontSize: 15.0 * f,
  height: 1.4,
);

void initTargets(
    GlobalKey keyButton1, GlobalKey keyButton2, GlobalKey keyButton3) {
  if (targets.length == 0) {
    targets.add(
      TargetFocus(
        identify: "Target 1",
        keyTarget: keyButton1,
        contents: [
          ContentTarget(
              align: AlignContent.bottom,
              child: Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 10 * y),
                    Text(
                      Translations.textStatic('mainExpenselistTutorial1', 'Spending_T'),
                      style: titleStyle,
                      textAlign: TextAlign.center,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0 * y),
                      child: Text(
                        Translations.textStatic('mainExpenselistTutorial2', 'Spending_T'),
                        style: bodyStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 280 * y),
                    Column(
                      children: <Widget>[
                        Image.asset(
                          "images/tab_symbol.png",
                          height: 100 * y,
                        ),
                        SizedBox(height: 20 * y),
                        Text(
                          Translations.textStatic('swipeToNavigate', 'Spending_T'),
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 20 * f),
                        ),
                      ],
                    ),
                  ],
                ),
              ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: "Target 2",
        keyTarget: keyButton2,
        contents: [
          ContentTarget(
              align: AlignContent.bottom,
              child: Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 10 * y),
                    Text(
                      Translations.textStatic('mainExpenselistTutorial3', 'Spending_T'),
                      style: titleStyle,
                      textAlign: TextAlign.center,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0 * y),
                      child: Text(
                        Translations.textStatic('mainExpenselistTutorial4', 'Spending_T'),
                        style: bodyStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 30 * y),
                  ],
                ),
              ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: "Target 3",
        keyTarget: keyButton3,
        contents: [
          ContentTarget(
              align: AlignContent.bottom,
              child: Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 10 * y),
                    Text(
                     Translations.textStatic('mainExpenselistTutorial5', 'Spending_T'),
                      style: titleStyle,
                      textAlign: TextAlign.center,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0 * y),
                      child: Text(
                        Translations.textStatic('mainExpenselistTutorial6', 'Spending_T'),
                        style: bodyStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 30 * y),
                  ],
                ),
              ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );
  }
}

void showTutorial(BuildContext context) {
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(statusBarColor: ColorPallet.darkTextColor),
  );
  showGestureLogo = true;
  TutorialCoachMark(
    context,
    targets: targets,
    colorShadow: ColorPallet.darkTextColor,
    textSkip: Translations.textStatic('skip', '?'),    
    paddingFocus: 10,
    textStyleSkip: TextStyle(color: Colors.white, fontSize: 16 * f),
    alignSkip: Alignment.topLeft,
    opacityShadow: .98,
    clickSkip: () {
      showGestureLogo = false;
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
      );
    },
    finish: () {
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
      );
    },
    clickTarget: (TargetFocus target) {
      showGestureLogo = false;
    },
  )..show();
}
