import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/animated_focus_light.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/content_target.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/target_focus.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/tutorial_coach_mark.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:budget_onderzoek/translations/translations.dart';

List<TargetFocus> targets = List();

TextStyle titleStyle = TextStyle(
  fontWeight: FontWeight.w700,
  color: Colors.white,
  fontSize: 22.0 * f,
  height: 1.4,
);
TextStyle bodyStyle = TextStyle(
  fontWeight: FontWeight.w400,
  color: Colors.white,
  fontSize: 15.0 * f,
  height: 1.4,
);

void initTargets(GlobalKey keyButton1, GlobalKey keyButton2,
    GlobalKey keyButton3, GlobalKey keyButton4, BuildContext context) {
  if (targets.length == 0) {
    targets.add(
      TargetFocus(
        identify: "blue",
        keyTarget: keyButton1,
        contents: [
          ContentTarget(
              child: Container(
            height: MediaQuery.of(context).size.height - 150 * y,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(children: <Widget>[
                  Text(
                    Translations.textStatic('mainOverviewTutorial1', 'Calendar_T'),
                    style: titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 7 * y),
                  Text(
                    Translations.textStatic('mainOverviewTutorial2', 'Calendar_T'),
                    style: bodyStyle,
                    textAlign: TextAlign.center,
                  ),
                ]),
                Column(
                  children: <Widget>[
                    Text(
                      Translations.textStatic('mainOverviewTutorial3', 'Calendar_T'),
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                          fontSize: 22.0 * f,
                          height: 1.4,
                          shadows: [
                            Shadow(blurRadius: 1, color: Colors.black)
                          ]),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 7 * y),
                    Text(
                      Translations.textStatic('mainOverviewTutorial4', 'Calendar_T'),
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                          fontSize: 15.0 * f,
                          height: 1.4,
                          shadows: [
                            Shadow(blurRadius: 1, color: Colors.black)
                          ]),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    SizedBox(height: 30 * y),
                    Image.asset(
                      "images/tab_symbol.png",
                      height: 100 * y,
                    ),
                  ],
                ),
              ],
            ),
          ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: "normal",
        keyTarget: keyButton2,
        contents: [
          ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                children: <Widget>[
                  Text(
                    Translations.textStatic('mainOverviewTutorial5', 'Calendar_T'),
                    style: titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 7 * y),
                  Text(
                    Translations.textStatic('mainOverviewTutorial6', 'Calendar_T'),
                    style: bodyStyle,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          )
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    if (LanguageSetting.key == "nl" ||
        LanguageSetting.key == "en") {
      targets.add(
        TargetFocus(
          identify: "normal",
          keyTarget: keyButton3,
          contents: [
            ContentTarget(
                align: AlignContent.top,
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Text(
                        Translations.textStatic('mainOverviewTutorial7', 'Calendar_T'),
                        style: titleStyle,
                        textAlign: TextAlign.center,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0 * y),
                        child: Text(
                          Translations.textStatic('mainOverviewTutorial8', 'Calendar_T'),
                          style: bodyStyle,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(height: 15 * y),
                    ],
                  ),
                ))
          ],
          shape: ShapeLightFocus.RRect,
        ),
      );
    }

    targets.add(
      TargetFocus(
        identify: "normal",
        keyTarget: keyButton4,
        contents: [
          ContentTarget(
              align: AlignContent.top,
              child: Container(
                child: Column(
                  children: <Widget>[
                    Text(
                      Translations.textStatic('mainOverviewTutorial9', 'Calendar_T'),
                      style: titleStyle,
                      textAlign: TextAlign.center,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0 * y),
                      child: Text(
                        Translations.textStatic('mainOverviewTutorial10', 'Calendar_T'),
                        style: bodyStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 30 * y),
                  ],
                ),
              ))
        ],
        shape: ShapeLightFocus.Circle,
      ),
    );
  }
}

void showTutorial(BuildContext context) {
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(statusBarColor: ColorPallet.darkTextColor),
  );
  showGestureLogo = true;
  TutorialCoachMark(
    context,
    targets: targets,
    colorShadow: ColorPallet.darkTextColor,
    textSkip: Translations.textStatic('skip', '?'),
    paddingFocus: 10,
    textStyleSkip: TextStyle(color: Colors.white, fontSize: 16 * f),
    alignSkip: Alignment.topLeft,
    opacityShadow: .98,
    clickSkip: () {
      showGestureLogo = false;
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
      );
    },
    finish: () {
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
      );
    },
    clickTarget: (TargetFocus target) {
      showGestureLogo = false;
    },
  )..show();
}
