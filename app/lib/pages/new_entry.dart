import 'dart:async';
import 'package:budget_onderzoek/resources/ssb_color_palett.dart';
import 'package:budget_onderzoek/util/algorithms/quick_search.dart';
import 'package:budget_onderzoek/util/algorithms/string_matching_products.dart';
import 'package:budget_onderzoek/util/algorithms/string_matching_shops.dart';
import 'package:budget_onderzoek/pages/search.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/tutorials/new_entry_tutorial.dart';
import 'package:budget_onderzoek/util/group_products.dart';
import 'package:budget_onderzoek/util/textfield_workaround.dart';
import 'package:budget_onderzoek/util/user_input.dart';
import 'package:budget_onderzoek/widgets/close_page_warning_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:budget_onderzoek/widgets/custom_flutter/custom_dialog.dart'
    as customDialog;
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/translations/international.dart';

Translations translations;

ScrollController _scrollControllerPage;
TextEditingControllerWorkaroud textController;

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
GlobalKey keyButton3 = GlobalKey();
GlobalKey keyButton4 = GlobalKey();
GlobalKey keyButton5 = GlobalKey();
GlobalKey keyButton6 = GlobalKey();
GlobalKey keyButton7 = GlobalKey();
GlobalKey keyButton8 = GlobalKey();

class NewEntryPage extends StatefulWidget {
  @override
  _NewEntryPageState createState() => _NewEntryPageState();
}

class _NewEntryPageState extends State<NewEntryPage> {
  void showInitialTutorial(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String status = prefs.getString("newEntryTutorial") ?? "";
    if (status == "") {
      prefs.setString("newEntryTutorial", "shown");
      showTutorial(context);
    }
  }

  @override
  void initState() {
    initTargets(keyButton1, keyButton2, keyButton3, keyButton4, keyButton5,
        keyButton6, keyButton7, keyButton8);
    super.initState();
    textController = TextEditingControllerWorkaroud();

    Future.delayed(Duration(milliseconds: 200), () {
      showInitialTutorial(context);
    });
  }

  Future<void> loadData() async {
    StringMatchingProducts.loadProductTable();
    QuickSearch.loadQuickSearchTable();
    StringMatchingShops.loadShopTable();
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Manual_Entry');
    _scrollControllerPage = ScrollController();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: SSBColorPalette.GREEN_4,
    ));

    loadData();

    return WillPopScope(
      onWillPop: () async {
        await showDialog(
          context: context,
          builder: (BuildContext context) {
            return ClosingPageWarning();
          },
        );
        return false;
      },
      child: Theme(
        data: ThemeData(
          // primaryColor: SSBColorPalette.GREEN_4,
          // accentColor: SSBColorPalette.GREEN_4,
          primaryColor: SSBColorPalette.GREEN_4,
          accentColor: SSBColorPalette.GREEN_1,
        ),
        child: Scaffold(
          appBar: AppBar(
            leading: InkWell(
                onTap: () async {
                  await showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return ClosingPageWarning();
                    },
                  );
                },
                child: Icon(Icons.close, color: Colors.white, size: 28 * x)),
            titleSpacing: 0,
            title: _EntryPageTitleWidget(),
            backgroundColor: SSBColorPalette.GREEN_4,
          ),
          body: _EntryPageBodyWidget(),
        ),
      ),
    );
  }
}

class _EntryPageTitleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Row(
      children: <Widget>[
        Text(
          translations.text('addReceipt'),
          //translations.text('newTransaction'),
          style: TextStyle(color: Colors.white, fontSize: 22 * x),
        ),
        Expanded(
          child: Center(
            child: InkWell(
                key: keyButton1,
                child: Icon(Icons.info, color: Colors.white, size: 28.0 * x),
                onTap: () {
                  showTutorial(context);
                }),
          ),
        ),
        ScopedModelDescendant<NewTransactionModel>(
          builder: (context, child, model) => Container(
            height: 32 * y,
            child: RaisedButton(
              key: keyButton8,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0 * x),
              ),
              color: model.productCounter > 0 && model.store != ''
                  ? ColorPallet.darkTextColor
                  : ColorPallet.midGray,
              onPressed: () async {
                if (model.productCounter > 0 && model.storeInfoComplete) {
                  NewTransactionModel.of(context).addNewTransAction();
                  SystemChrome.setSystemUIOverlayStyle(
                    SystemUiOverlayStyle(
                        statusBarColor: ColorPallet.primaryColor),
                  );
                  Navigator.of(context).pop();
                } else {
                  String warning = "";
                  if (!(model.productCounter > 0) && !model.storeInfoComplete) {
                    warning = translations.text('warningIncomplete');
                  } else {
                    if (!(model.productCounter > 0)) {
                      warning = translations.text ('warningNoProduct');
                    }
                    if (!model.storeInfoComplete) {
                      warning = translations.text ('warningNoShopInfo');
                      _scrollControllerPage.animateTo(
                        0,
                        curve: Curves.easeOut,
                        duration: const Duration(milliseconds: 300),
                      );
                    }
                  }

                  await showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          titlePadding: EdgeInsets.all(0),
                          title: Container(
                            color: SSBColorPalette.GREEN_4,
                            padding: EdgeInsets.all(20),
                            child: Row(
                              children: <Widget>[
                                Text(
                                  translations.text ('informationIncomplete'),
                                  style: TextStyle(
                                      fontSize: 20 * f,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                          content: Text(warning,
                              style: TextStyle(
                                  fontSize: 17 * f,
                                  fontWeight: FontWeight.w500,
                                  color: ColorPallet.darkTextColor)),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(translations.text ('ok'),
                                  style: TextStyle(color: SSBColorPalette.GREEN_4)),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            )
                          ],
                        );
                      });
                }
              },
              child: Text(
                translations.text ('complete'),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16 * x,
                ),
              ),
            ),
          ),
        ),
        SizedBox(width: 20 * x),
      ],
    );
  }
}

class _EntryPageBodyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Expanded(
            child: ScopedModelDescendant<NewTransactionModel>(
              builder: (context, child, model) {
                var productGroups =
                    GroupProducts.getProductGroups(model.products);

                return Column(
                  children: <Widget>[
                    productGroups.length == 0
                        ? Column(children: <Widget>[
                            _TransactionDetailsWidget(),
                            _AddNewItemButton()
                          ])
                        : Container(),
                    Expanded(
                      child: ListView.builder(
                        controller: _scrollControllerPage,
                        itemCount: productGroups.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Center(
                              child: _ProductTile(
                                  productGroups[index],
                                  index == productGroups.length - 1,
                                  index == 0));
                        },
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
          _ReceiptBottomWidget(),
        ],
      ),
    );
  }
}

class _TransactionDetailsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(bottom: 12 * y),
          width: 400 * x,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(5.0 * x),
            ),
            color: Colors.white,
          ),
          child: ScopedModelDescendant<NewTransactionModel>(
            builder: (context, child, model) => Column(
              children: <Widget>[
                SizedBox(
                    height: 15 * y, width: MediaQuery.of(context).size.width),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text("*" + translations.text ('required'),
                        style: TextStyle(
                            color: ColorPallet.darkTextColor,
                            fontWeight: FontWeight.w700,
                            fontSize: 11 * f)),
                    SizedBox(width: 27 * x),
                  ],
                ),
                SizedBox(height: 2 * y),
                _StoreInput(),
                _DateInput(),
                _UserInputFieldSwitches(),
              ],
            ),
          ),
        ),
        Text(
          translations.text ('receiptItems'),
          style: TextStyle(
              color: ColorPallet.darkTextColor,
              fontWeight: FontWeight.w700,
              fontSize: 17 * f),
        ),
        SizedBox(height: 15 * y),
      ],
    );
  }
}

class _StoreInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);
    return Stack(
      key: keyButton2,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0 * x)),
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: 10.0 * x, vertical: 10 * y),
              child: InkWell(
                onTap: () async {
                  await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SearchWidget(false),
                    ),
                  );
                },
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.store,
                            color: ColorPallet.darkTextColor, size: 27 * x),
                        SizedBox(width: 10.0 * x),
                        Container(
                          width: 305 * x,
                          child: Text(
                              newTransaction.store != ""
                                  ? newTransaction.store
                                  : translations.text ('enterShop'),
                              style: TextStyle(
                                  color: newTransaction.store != ""
                                      ? ColorPallet.darkTextColor
                                      : ColorPallet.midGray,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18 * f),
                              overflow: TextOverflow.ellipsis),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              translations.text ('shop') + "*",
              style: TextStyle(
                  color: ColorPallet.darkTextColor,
                  fontWeight: FontWeight.w700,
                  fontSize: 14 * f),
            ),
          ),
        ),
      ],
    );
  }
}

class _DateInput extends StatefulWidget {
  @override
  __DateInputState createState() => __DateInputState();
}

class __DateInputState extends State<_DateInput> {
  NewTransactionModel newTransaction;
  String hintText = translations.text ('today');
  DateTime initialDate =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

  Future<Null> _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      locale: International.languageFromId(LanguageSetting.key).locale,
      context: context,
      initialDate: initialDate,
      firstDate: DateTime(2019),
      lastDate: DateTime(2101),
    );
    picked = DateTime(picked.year, picked.month, picked.day);
    if (picked != null) {
      newTransaction.date = DateFormat('yyyy-MM-dd').format(picked);
      newTransaction.notifyListeners();
    }
  }

  String getDateString(String date) {
    DateTime today =
        DateTime(initialDate.year, initialDate.month, initialDate.day);
    DateTime yesterday =
        DateTime(initialDate.year, initialDate.month, initialDate.day - 1);
    DateTime tomorrow =
        DateTime(initialDate.year, initialDate.month, initialDate.day + 1);

    if (date == DateFormat('yyyy-MM-dd').format(today)) {
      return translations.text ('today');
    } else if (date == DateFormat('yyyy-MM-dd').format(yesterday)) {
      return translations.text ('yesterday');
    } else if (date == DateFormat('yyyy-MM-dd').format(tomorrow)) {
      return translations.text ('tomorrow');
    } else {
      return DateFormat('EEEE, d MMMM', International.languageFromId(LanguageSetting.key).localeKey)
          .format(DateTime.parse(date));
    }
  }

  @override
  Widget build(BuildContext context) {
    newTransaction = NewTransactionModel.of(context);
    return Stack(
      key: keyButton3,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0)),
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: 10.0 * x, vertical: 10 * y),
              child: InkWell(
                onTap: () async {
                  _selectDate(context);
                },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.today,
                        color: ColorPallet.darkTextColor, size: 27 * x),
                    SizedBox(width: 10.0 * x),
                    ScopedModelDescendant<NewTransactionModel>(
                      builder: (context, child, model) => Text(
                          getDateString(model.date),
                          style: TextStyle(
                              color: ColorPallet.darkTextColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 18 * f)),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(translations.text ('date') + "*",
                style: TextStyle(
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w700,
                    fontSize: 14 * f)),
          ),
        ),
      ],
    );
  }
}

class _UserInputFieldSwitches extends StatefulWidget {
  @override
  __UserInputFieldSwitchesState createState() =>
      __UserInputFieldSwitchesState();
}

class __UserInputFieldSwitchesState extends State<_UserInputFieldSwitches> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      key: keyButton4,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0)),
            child: ScopedModelDescendant<NewTransactionModel>(
              builder: (context, child, model) => Row(
                children: <Widget>[
                  SizedBox(
                    height: 26 * y,
                  ),
                  Container(
                    width: 364 * x,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                translations.text ('abroad'),
                                style: TextStyle(
                                    color: ColorPallet.darkTextColor,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14 * f),
                              ),
                              // SizedBox(
                              //   width: 5.0 * x,
                              // ),
                              Switch(
                                activeColor: SSBColorPalette.GREEN_4,
                                value: model.abroadExpense,
                                onChanged: (val) {
                                  setState(() {
                                    model.abroadExpense = !model.abroadExpense;
                                  });
                                },
                              )
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                translations.text ('online'),
                                style: TextStyle(
                                    color: ColorPallet.darkTextColor,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14 * f),
                              ),
                              // SizedBox(
                              //   width: 5.0 * x,
                              // ),
                              Switch(
                                activeColor: SSBColorPalette.GREEN_4,
                                value: model.onlineExpense,
                                onChanged: (val) {
                                  setState(() {
                                    model.onlineExpense = !model.onlineExpense;
                                  });
                                },
                              ),
                            ],
                          )
                        ]),
                  ),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(translations.text ('others2'),
                style: TextStyle(
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w700,
                    fontSize: 14 * f)),
          ),
        ),
      ],
    );
  }
}

class _ProductDialog extends StatefulWidget {
  @override
  __ProductDialogState createState() => __ProductDialogState();
}

class __ProductDialogState extends State<_ProductDialog> {
  void resetValues() {
    NewTransactionModel.of(context).isDiscountItem = false;
    NewTransactionModel.of(context).price = "";
    NewTransactionModel.of(context).itemMultiplier = "1";
    textController.setTextAndPosition('');
  }

  String computeTotalPrice(NewTransactionModel model) {
    String price;

    if (model.price != "" &&
        model.price != "-" &&
        model.price != "." &&
        model.price != ",") {
      price = " €" +
          (double.parse(model.price.replaceAll(",", ".")) *
                  int.parse(model.itemMultiplier))
              .toStringAsFixed(2);
    } else {
      price = " €0.00";
    }
    return translations.text ('totalPrice') + price;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        resetValues();
        return Future.value(true);
      },
      child: customDialog.AlertDialog(
          contentPadding: EdgeInsets.all(0),
          content: Container(
            width: 500 * x,
            height: 350 * y,
            child: ScopedModelDescendant<NewTransactionModel>(
              builder: (context, child, model) => Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(width: 35 * x),
                          Expanded(child: Container()),
                          Text(translations.text ('addingNewItems'),
                              style: TextStyle(
                                fontSize: 22 * f,
                                fontWeight: FontWeight.w700,
                                color: ColorPallet.darkTextColor,
                              )),
                          Expanded(child: Container()),
                          InkWell(
                              onTap: () {
                                resetValues();
                                Navigator.of(context).pop();
                              },
                              child: Icon(Icons.close,
                                  color: ColorPallet.darkTextColor)),
                          SizedBox(width: 15 * x)
                        ]),
                    Column(children: <Widget>[
                      _ProductInput(),
                      _PriceInput(),
                      SizedBox(height: 10 * y),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(width: 27 * x),
                            InkWell(
                              onTap: () {
                                model.subtractItemMultiplier();
                              },
                              child: Icon(Icons.indeterminate_check_box,
                                  size: 30 * x,
                                  color: ColorPallet.darkTextColor),
                            ),
                            SizedBox(width: 5 * x),
                            Container(
                              width: 35 * x,
                              child: Center(
                                child: Text(
                                  model.itemMultiplier + "x",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      fontSize: 20 * f,
                                      color: ColorPallet.darkTextColor),
                                ),
                              ),
                            ),
                            SizedBox(width: 5 * x),
                            InkWell(
                              onTap: () {
                                model.addItemMultiplier();
                              },
                              child: Icon(Icons.add_box,
                                  size: 30 * x,
                                  color: ColorPallet.darkTextColor),
                            ),
                            Expanded(child: Container()),
                            Text(translations.text ('discountReturnEtc'),
                                style: TextStyle(
                                  fontSize: 12 * f,
                                  fontWeight: FontWeight.w500,
                                  color: ColorPallet.darkTextColor,
                                )),
                            Switch(
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap,
                              onChanged: (value) {
                                setState(() {
                                  bool isDiscount =
                                      !NewTransactionModel.of(context)
                                          .isDiscountItem;
                                  String currentInput =
                                      NewTransactionModel.of(context).price;
                                  bool inputIsNegative = currentInput.length > 0
                                      ? currentInput.substring(0, 1) == '-'
                                      : false;

                                  if (isDiscount && !inputIsNegative) {
                                    NewTransactionModel.of(context).price =
                                        "-" + currentInput;
                                  } else if (!isDiscount && inputIsNegative) {
                                    NewTransactionModel.of(context).price =
                                        currentInput.substring(
                                            1, currentInput.length);
                                  }
                                  textController.setTextAndPosition(
                                      NewTransactionModel.of(context).price);
                                  NewTransactionModel.of(context)
                                      .isDiscountItem = isDiscount;
                                });
                              },
                              value: model.isDiscountItem,
                            ),
                            SizedBox(width: 13 * x)
                          ]),
                    ]),
                    Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            computeTotalPrice(model),
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 18 * f,
                                backgroundColor: SSBColorPalette.GREEN_1,
                                color: ColorPallet.darkTextColor),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 31 * y,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6.5 * x),
                        ),
                        color: model.productInfoComplete
                            ? ColorPallet.darkTextColor
                            : ColorPallet.midGray,
                        child: Text(
                          translations.text ('add'),
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 17.0 * f),
                        ),
                        onPressed: () async {
                          if (model.price != '') {
                            if (model.productInfoComplete) {
                              NewTransactionModel.of(context).price =
                                  double.parse(NewTransactionModel.of(context)
                                          .price
                                          .replaceAll(',', '.'))
                                      .toStringAsFixed(2);
                              await model.addNewProduct();
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                            }
                            resetValues();
                            Navigator.of(context).pop();
                            _scrollControllerPage.animateTo(
                              (NewTransactionModel.of(context).products.length *
                                      64 *
                                      y) +
                                  500 * y,
                              curve: Curves.easeOut,
                              duration: const Duration(milliseconds: 300),
                            );
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }
}

class _ProductInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0 * x)),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 10.0 * x, vertical: 10.0 * y),
              child: InkWell(
                onTap: () async {
                  String result = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SearchWidget(true)));
                  if (result != null) {
                    newTransaction.product = result;
                    newTransaction.isProductInfoComplete();
                  }
                },
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.label,
                            color: ColorPallet.darkTextColor, size: 27 * x),
                        SizedBox(width: 10.0 * x),
                        Container(
                          width: 266 * x,
                          child: Text(
                            newTransaction.product != null
                                ? newTransaction.product
                                : translations.text ('enterProductService'),
                            style: TextStyle(
                                color: newTransaction.product != ""
                                    ? ColorPallet.darkTextColor
                                    : ColorPallet.midGray,
                                fontWeight: FontWeight.w500,
                                fontSize: 18 * f),
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    ),
                    newTransaction.productCategory == ""
                        ? Container()
                        : Column(
                            children: <Widget>[
                              SizedBox(height: 5 * y),
                              Row(
                                children: <Widget>[
                                  SizedBox(width: 6.0 * x),
                                  Icon(Icons.category,
                                      color: SSBColorPalette.GREEN_4, size: 16 * x),
                                  SizedBox(width: 16.7 * x),
                                  Container(
                                    width: 266 * x,
                                    child: Text(newTransaction.productCategory,
                                        style: TextStyle(
                                            color: SSBColorPalette.GREEN_4,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14 * f),
                                        overflow: TextOverflow.ellipsis),
                                  )
                                ],
                              ),
                            ],
                          ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              translations.text ('productService') + "*",
              style: TextStyle(
                  color: ColorPallet.darkTextColor,
                  fontWeight: FontWeight.w700,
                  fontSize: 14.0 * f),
            ),
          ),
        ),
      ],
    );
  }
}

class _PriceInput extends StatefulWidget {
  @override
  __PriceInputState createState() => __PriceInputState();
}

class __PriceInputState extends State<_PriceInput> {
  FocusNode myFocusNode;
  String price = "";

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }

  void checkUserInput(String newPrice, BuildContext context) {
    if (newPrice.length == 0) {
      NewTransactionModel.of(context).isDiscountItem = false;
      NewTransactionModel.of(context).price = '';
      NewTransactionModel.of(context).notifyListeners();
    }

    if (!UserInput.valueValidator(newPrice)) {
      Toast.show(translations.text ('invalidInput'), context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      textController.setTextAndPosition(price);
    } else {
      price = newPrice;
      NewTransactionModel.of(context).price = price;
      NewTransactionModel.of(context).isProductInfoComplete();
    }
    FocusScope.of(context).requestFocus(myFocusNode);
    if (newPrice.substring(0, 1) == "-") {
      NewTransactionModel.of(context).isDiscountItem = true;
    } else {
      NewTransactionModel.of(context).isDiscountItem = false;
    }
    NewTransactionModel.of(context).notifyListeners();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 7 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0 * x)),
            child: Row(
              children: <Widget>[
                SizedBox(width: 10.0 * x),
                Icon(Icons.local_offer,
                    color: ColorPallet.darkTextColor, size: 27 * x),
                // SizedBox(width: 3.0),
                Container(
                  alignment: Alignment.centerLeft,
                  width: 284 * x,
                  height: 38 * y,
                  margin: EdgeInsets.only(top: 2 * y),
                  child: TextField(
                    autofocus: true,
                    focusNode: myFocusNode,
                    controller: textController,
                    style: TextStyle(
                        color: ColorPallet.darkTextColor,
                        fontWeight: FontWeight.w500,
                        fontSize: 18 * f),
                    autocorrect: false,
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    onChanged: (newPrice) {
                      setState(() {
                        checkUserInput(newPrice, context);
                      });
                    },
                    onTap: () {
                      _scrollControllerPage.animateTo(
                        _scrollControllerPage.position.maxScrollExtent,
                        curve: Curves.easeOut,
                        duration: const Duration(milliseconds: 300),
                      );
                    },
                    onSubmitted: (value) {
                      if (NewTransactionModel.of(context).price != "") {
                        price = double.parse(price.replaceAll(',', '.'))
                            .toStringAsFixed(2);
                        textController.text = price;
                        NewTransactionModel.of(context).price = price;
                        NewTransactionModel.of(context).isProductInfoComplete();
                      }
                    },
                    decoration: InputDecoration(
                        hintText: NewTransactionModel.of(context).price != ""
                            ? NewTransactionModel.of(context).price
                            : translations.text ('enterPrice'),
                        hintStyle: TextStyle(
                            color: ColorPallet.midGray,
                            fontWeight: FontWeight.w500,
                            fontSize: 18 * f),
                        filled: true,
                        fillColor: Colors.transparent,
                        border: InputBorder.none,
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 10 * x)),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: 48,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              translations.text ('price') + "*",
              style: TextStyle(
                  color: ColorPallet.darkTextColor,
                  fontWeight: FontWeight.w700,
                  fontSize: 14 * f),
            ),
          ),
        ),
      ],
    );
  }
}

class _AddNewItemButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);
    return Column(
      key: keyButton5,
      children: <Widget>[
        SizedBox(height: 5 * y),
        InkWell(
          onTap: () async {
            await Navigator.push(context,
                MaterialPageRoute(builder: (context) => SearchWidget(true)));

            if (newTransaction.product != null) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return _ProductDialog();
                  });
            }
          },
          child: DottedBorder(
            color: ColorPallet.lightGray,
            strokeWidth: 1.8,
            dashPattern: [6, 6],
            radius: Radius.circular(10),
            strokeCap: StrokeCap.round,
            borderType: BorderType.RRect,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: Container(
                height: 50 * y,
                width: 365 * x,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width: 21 * x),
                    Text(
                      translations.text ('addNewServiceProductOrDiscount'),
                      style: TextStyle(
                          color: ColorPallet.darkTextColor,
                          fontWeight: FontWeight.w600,
                          fontSize: 15 * f),
                    ),
                    SizedBox(width: 31 * x),
                    Icon(
                      Icons.add,
                      color: ColorPallet.darkTextColor,
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: 9 * y),
      ],
    );
  }
}

class _ProductTile extends StatefulWidget {
  _ProductTile(this.productInfo, this.isLastItem, this.isFirstItem);

  final bool isFirstItem;
  final bool isLastItem;
  final Map<String, String> productInfo;

  @override
  __ProductTileState createState() => __ProductTileState();
}

class __ProductTileState extends State<_ProductTile> {
  PointerDownEvent recognizeTap;
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    String itemPrice = widget.productInfo['itemPrice'].toString();
    String totalPrice = widget.productInfo['totalPrice'].toString();
    String product = widget.productInfo['product'];
    String productCategory = widget.productInfo['productCategory'];
    String itemCount = widget.productInfo['itemCount'];
    return Column(
      children: <Widget>[
        widget.isFirstItem ? _TransactionDetailsWidget() : Container(),
        Container(
          height: 57.5 * y,
          margin: EdgeInsets.symmetric(vertical: 4 * y),
          width: 370 * x,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(12.0 * x),
              ),
              color: Colors.white,
              border: Border.all(
                  width: 1.5,
                  color: selected
                      ? ColorPallet.darkTextColor
                      : ColorPallet.lightGray)),
          child: Listener(
            onPointerDown: (v) {
              recognizeTap = v;
            },
            onPointerUp: (v) {
              bool _dx =
                  (recognizeTap.localPosition.dx - v.localPosition.dx).abs() <
                      18;
              bool _dy =
                  (recognizeTap.localPosition.dy - v.localPosition.dy).abs() <
                      18;
              if (recognizeTap.pointer == v.pointer && _dx && _dy) {
                setState(() {
                  selected = true;
                });
              }
            },
            child: PopupMenuButton<String>(
              onCanceled: () {
                setState(() {
                  selected = false;
                });
              },
              onSelected: (v) {
                setState(() {
                  selected = false;
                });
              },
              offset: Offset(20 * x, 35 * y),
              itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                PopupMenuItem<String>(
                  value: 'duplicate',
                  child: InkWell(
                    onTap: () {
                      print('duplicate ' +
                          DateTime.now().millisecondsSinceEpoch.toString());
                      NewTransactionModel.of(context)
                          .duplicateProductGroup(widget.productInfo);
                      _scrollControllerPage.animateTo(
                          (NewTransactionModel.of(context).products.length *
                                  64 *
                                  y) +
                              500 * y,
                          curve: Curves.easeOut,
                          duration: const Duration(milliseconds: 300));
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: <Widget>[
                        SizedBox(width: 5 * x),
                        Icon(
                          Icons.content_copy,
                          color: ColorPallet.darkTextColor,
                          size: 20 * x,
                        ),
                        SizedBox(width: 17 * x),
                        Text(translations.text ('duplicate'),
                            style: TextStyle(
                                fontSize: 16 * f,
                                color: ColorPallet.darkTextColor,
                                fontWeight: FontWeight.w500)),
                      ],
                    ),
                  ),
                ),
                PopupMenuItem<String>(
                  value: 'modify',
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      NewTransactionModel.of(context)
                          .modifyProductGroup(widget.productInfo);
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return _ProductDialog();
                          });
                    },
                    child: Row(
                      children: <Widget>[
                        SizedBox(width: 5 * x),
                        Icon(
                          Icons.edit,
                          color: ColorPallet.darkTextColor,
                          size: 20 * x,
                        ),
                        SizedBox(width: 17 * x),
                        Text(translations.text ('modify'),
                            style: TextStyle(
                                fontSize: 16 * f,
                                color: ColorPallet.darkTextColor,
                                fontWeight: FontWeight.w500)),
                      ],
                    ),
                  ),
                ),
                PopupMenuItem<String>(
                  value: 'delete',
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      NewTransactionModel.of(context)
                          .deleteProduct(widget.productInfo);
                    },
                    child: Row(
                      children: <Widget>[
                        SizedBox(width: 5 * x),
                        Icon(
                          Icons.delete,
                          color: ColorPallet.darkTextColor,
                          size: 20 * x,
                        ),
                        SizedBox(width: 17 * x),
                        Text(translations.text ('deleteItem'),
                            style: TextStyle(
                                fontSize: 16 * f,
                                color: ColorPallet.darkTextColor,
                                fontWeight: FontWeight.w500)),
                      ],
                    ),
                  ),
                ),
              ],
              child: Row(
                children: <Widget>[
                  SizedBox(width: 5 * x),
                  Container(
                    width: 60 * x,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          itemCount + "x",
                          style: TextStyle(
                              fontSize: 14 * f,
                              fontWeight: FontWeight.w600,
                              color: ColorPallet.darkTextColor),
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 3 * y),
                        Text(
                          itemPrice,
                          style: TextStyle(
                              fontSize: 14 * f,
                              fontWeight: FontWeight.w800,
                              color: ColorPallet.darkTextColor),
                          overflow: TextOverflow.visible,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 5 * x),
                  Container(
                    width: 200 * x,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(product,
                            style: TextStyle(
                                fontSize: 16.5 * f,
                                fontWeight: FontWeight.w600,
                                color: ColorPallet.darkTextColor),
                            overflow: TextOverflow.ellipsis),
                        Text(productCategory,
                            style: TextStyle(
                                fontSize: 14 * f,
                                fontWeight: FontWeight.w600,
                                color: ColorPallet.midGray),
                            overflow: TextOverflow.ellipsis),
                      ],
                    ),
                  ),
                  Expanded(child: Container()),
                  Container(
                    width: 70 * x,
                    child: Text(
                      double.parse(totalPrice).toStringAsFixed(2),
                      style: TextStyle(
                          fontSize: 18 * f,
                          fontWeight: FontWeight.w600,
                          color: ColorPallet.darkTextColor),
                      maxLines: 1,
                      overflow: TextOverflow.visible,
                      textAlign: TextAlign.end,
                    ),
                  ),
                  SizedBox(width: 15 * x),
                ],
              ),
            ),
          ),
        ),
        widget.isLastItem ? _AddNewItemButton() : Container(),
      ],
    );
  }
}

class _ReceiptBottomWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<NewTransactionModel>(
      builder: (context, child, model) => Column(
        children: <Widget>[
          Container(
              height: 1 * y,
              color: ColorPallet.darkTextColor.withOpacity(0.27)),
          SizedBox(height: 10 * y),
          Row(
            children: <Widget>[
              SizedBox(width: 18 * x),
              _DiscountInput(),
              SizedBox(width: 17 * x),
              Column(
                key: keyButton7,
                children: <Widget>[
                  Text(
                    translations.text ('totalAmount'),
                    style: TextStyle(
                      color: ColorPallet.darkTextColor,
                      fontWeight: FontWeight.w700,
                      fontSize: 18 * f,
                    ),
                  ),
                  SizedBox(height: 2 * y),
                  Container(
                    width: 120 * x,
                    child: Center(
                      child: Text(
                        model.totalPrice.toStringAsFixed(2),
                        style: TextStyle(
                          color: ColorPallet.darkTextColor,
                          fontWeight: FontWeight.w700,
                          fontSize: 24 * f,
                        ),
                        overflow: TextOverflow.fade,
                      ),
                    ),
                  ),
                  SizedBox(height: 5 * y),
                ],
              )
            ],
          ),
          SizedBox(height: 5 * y)
        ],
      ),
    );
  }
}

class _DiscountInput extends StatefulWidget {
  @override
  _DiscountInputState createState() => _DiscountInputState();
}

class _DiscountInputState extends State<_DiscountInput> {
  String hintText = translations.text ('discount');

  void changeValue(String value, bool isPercentage) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);
    setState(() {
      if (double.parse(value) == 0) {
        newTransaction.discountText = translations.text ('noDiscount');
        newTransaction.changeProductDiscountAmount(0);
      } else {
        if (isPercentage) {
          newTransaction.discountText = "-" + value + "%";
          newTransaction.changeProductDiscountPercentage(double.parse(value));
        } else {
          newTransaction.discountAmount = double.parse(value);
          newTransaction.changeProductDiscountAmount(double.parse(value));
          newTransaction.discountText = "-" + value;
        }
      }
    });
  }

  bool isNumeric(String str) {
    if (str == null) {
      return false;
    }
    if (str.substring(str.length - 1) == "%") {
      return true;
    }
    return double.tryParse(str) != null;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      key: keyButton6,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.54,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0)),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 10.0 * x, vertical: 10.0 * y),
              child: InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return _DiscountDialog(changeValue);
                      });
                },
                child: Row(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.tags,
                        color: ColorPallet.darkTextColor, size: 18 * f),
                    SizedBox(width: 17.0 * x),
                    ScopedModelDescendant<NewTransactionModel>(
                      builder: (context, child, model) => Text(
                          model.discountText,
                          style: TextStyle(
                              color: isNumeric(model.discountText)
                                  ? ColorPallet.darkTextColor
                                  : ColorPallet.midGray,
                              fontWeight: FontWeight.w500,
                              fontSize: 18 * f)),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(translations.text ('discount'),
                style: TextStyle(
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w700,
                    fontSize: 14 * f)),
          ),
        ),
      ],
    );
  }
}

class _DiscountDialog extends StatefulWidget {
  _DiscountDialog(this.changeValue);

  @override
  __DiscountDialogState createState() => __DiscountDialogState();

  final Function(String value, bool isPercentage) changeValue;
}

class __DiscountDialogState extends State<_DiscountDialog> {
  String percentageOnChangedInput = "0";
  String totalOnChangedInput = "0";

  final TextEditingController _percentageController =
      new TextEditingController();

  final TextEditingController _totalController = new TextEditingController();

  void checkPercentage(BuildContext context, String value) {
    value = UserInput.valueSanitizer(value);
    if (UserInput.percentageValidator(value)) {
      value = double.parse(value).toStringAsFixed(0);
      widget.changeValue(value, true);
      Navigator.pop(context);
    } else {
      Toast.show(translations.text ('invalidInput'), context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    }
  }

  void checkTotal(BuildContext context, String value) {
    value = UserInput.valueSanitizer(value);
    if (UserInput.valueValidator(value)) {
      widget.changeValue(value, false);
      Navigator.pop(context);
    } else {
      Toast.show(translations.text ('invalidInput'), context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.all(0),
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              color: SSBColorPalette.GREEN_4,
              height: 68 * y,
              width: 400 * x,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 20 * y),
                  Padding(
                    padding: EdgeInsets.only(left: 35.0 * x),
                    child: Text(translations.text ('enterDiscount'),
                        style: TextStyle(
                            fontSize: 25 * f,
                            fontWeight: FontWeight.w500,
                            color: Colors.white)),
                  ),
                ],
              ),
            ),
            SizedBox(height: 40 * y),
            Row(
              children: <Widget>[
                SizedBox(width: 53 * x),
                Column(
                  children: <Widget>[
                    Text(translations.text ('precentage'),
                        style: TextStyle(
                            fontSize: 17 * f,
                            fontWeight: FontWeight.w500,
                            color: ColorPallet.darkTextColor)),
                    SizedBox(height: 10 * y),
                    Row(
                      children: <Widget>[
                        Container(
                            width: 20 * x,
                            height: 45 * y,
                            child: Text("-",
                                style: TextStyle(
                                    fontSize: 16 * f,
                                    fontWeight: FontWeight.w700,
                                    color: ColorPallet.darkTextColor))),
                        Container(
                          width: 40 * x,
                          height: 70 * y,
                          child: TextField(
                            controller: _percentageController,
                            onSubmitted: (val) {
                              checkPercentage(
                                  context, percentageOnChangedInput);
                            },
                            onTap: () {
                              setState(() {
                                _totalController.clear();
                              });
                            },
                            onChanged: (val) {
                              setState(() {
                                percentageOnChangedInput = val;
                              });
                            },
                            keyboardType:
                                TextInputType.numberWithOptions(decimal: true),
                            decoration: InputDecoration(hintText: "0"),
                          ),
                        ),
                        Container(
                            width: 10 * x,
                            height: 45 * y,
                            child: Text("%",
                                style: TextStyle(
                                    fontSize: 16 * f,
                                    fontWeight: FontWeight.w700,
                                    color: ColorPallet.darkTextColor))),
                      ],
                    ),
                  ],
                ),
                SizedBox(width: 25 * x),
                Text(translations.text ('or'),
                    style: TextStyle(
                        fontSize: 17 * f,
                        fontWeight: FontWeight.w500,
                        color: ColorPallet.darkTextColor)),
                SizedBox(width: 35 * x),
                Column(
                  children: <Widget>[
                    Text(translations.text ('total2'),
                        style: TextStyle(
                            fontSize: 17 * f,
                            fontWeight: FontWeight.w500,
                            color: ColorPallet.darkTextColor)),
                    SizedBox(height: 10 * y),
                    Row(
                      children: <Widget>[
                        Container(
                            width: 20 * x,
                            height: 45 * y,
                            child: Text("-",
                                style: TextStyle(
                                    fontSize: 16 * f,
                                    fontWeight: FontWeight.w700,
                                    color: ColorPallet.darkTextColor))),
                        Container(
                          width: 40 * x,
                          height: 70 * y,
                          child: TextField(
                            controller: _totalController,
                            onSubmitted: (val) {
                              checkTotal(context, totalOnChangedInput);
                            },
                            onTap: () {
                              setState(() {
                                _percentageController.clear();
                              });
                            },
                            onChanged: (val) {
                              setState(() {
                                totalOnChangedInput = val;
                              });
                            },
                            keyboardType:
                                TextInputType.numberWithOptions(decimal: true),
                            decoration: InputDecoration(hintText: "0"),
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
            SizedBox(height: 30 * y),
            Container(
              width: 400 * x,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      translations.text ('cancel'),
                      style:
                          TextStyle(fontSize: 16 * f, color: SSBColorPalette.GREEN_4),
                    ),
                  ),
                  FlatButton(
                    onPressed: () {
                      if (percentageOnChangedInput != "0") {
                        checkPercentage(context, percentageOnChangedInput);
                      } else {
                        checkTotal(context, totalOnChangedInput);
                      }
                    },
                    child: Text(
                      translations.text ('add'),
                      style:
                          TextStyle(fontSize: 16 * f, color: SSBColorPalette.GREEN_4),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 10 * y)
          ],
        )
      ],
    );
  }
}
