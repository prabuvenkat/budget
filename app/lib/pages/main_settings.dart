import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/scoped_models/settings_scoped_model.dart';
import 'package:budget_onderzoek/util/login_setup.dart';
import 'package:budget_onderzoek/widgets/spotlight_tutorial/tutorials/main_settings_tutorial.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:device_info/device_info.dart';
import 'dart:io' show Platform;
import 'package:package_info/package_info.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/translations/international.dart';


Translations translations;

TextStyle headerText;
TextStyle settingText;
TextStyle valueText;
TextStyle valueTextBlue;

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
GlobalKey keyButton3 = GlobalKey();

class SettingsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SettingsPageState();
  }
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  void initState() {
    initTargets(keyButton1, keyButton2, keyButton3);
    super.initState();

    Future.delayed(
      Duration(milliseconds: 200),
      () {
        showInitialTutorial(context);
      },
    );
  }

  void showInitialTutorial(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String status = prefs.getString("mainSettingsTutorial") ?? "";
    if (status == "") {
      prefs.setString("mainSettingsTutorial", "shown");
      showTutorial(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Settings');
    headerText = TextStyle(
        color: ColorPallet.darkTextColor,
        fontSize: 18.0 * f,
        fontWeight: FontWeight.w700);
    settingText = TextStyle(
        color: ColorPallet.darkTextColor,
        fontSize: 16.0 * f,
        fontWeight: FontWeight.w600);
    valueText = TextStyle(
        color: ColorPallet.midGray,
        fontSize: 16.0 * f,
        fontWeight: FontWeight.w600);
    valueTextBlue = TextStyle(
        color: ColorPallet.primaryColor,
        fontSize: 16.0 * f,
        fontWeight: FontWeight.w600);

    return Column(
      children: <Widget>[
        _TopBarWidget(),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(bottom: 8.0),
            child: ListView(
              children: <Widget>[
                SizedBox(height: 8.0 * y),
                _NotificationWidget(),
                SizedBox(height: 8.0 * y),
                _LanguageWidget(),
                SizedBox(height: 8.0 * y),
                _ContactWidget(),
                SizedBox(height: 8.0 * y),
                _VersionWidget(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _TopBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorPallet.primaryColor,
      height: 50 * y,
      child: Row(
        children: <Widget>[
          SizedBox(width: 22.0 * x),
          ScopedModelDescendant<SettingsModel>(
              builder: (context, child, model) {
            return Text(
              translations.text('settings'),
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 22.0 * f,
                  fontWeight: FontWeight.w600),
            );
          }),
          SizedBox(width: 10 * x),
          InkWell(
              onTap: () {
                showTutorial(context);
              },
              child: Icon(Icons.info, color: Colors.white, size: 28.0 * x)),
        ],
      ),
    );
  }
}

class _NotificationWidget extends StatefulWidget {
  @override
  __NotificationWidgetState createState() => __NotificationWidgetState();
}

class __NotificationWidgetState extends State<_NotificationWidget> {
  @override
  Widget build(BuildContext context) {
    SettingsModel.of(context).loadSettings();
    return _SettingsBoxWidget(
        settingsWidget: Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
      child: ScopedModelDescendant<SettingsModel>(
        builder: (context, child, model) => Column(
          key: keyButton1,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.notifications,
                    color: ColorPallet.darkTextColor, size: 24.0 * x),
                SizedBox(width: 10.0 * x),
                Text(translations.text('notifications'), style: headerText),
              ],
            ),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translations.text('dailyReminders'), style: settingText),
                Switch(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  onChanged: (v) {
                    if (SettingsModel.of(context)
                            .flutterLocalNotificationsPlugin ==
                        null) {
                      SettingsModel.of(context).initNotificationPlugin();
                    }
                    model.changeNotificationSetting(context);
                  },
                  value: model.status,
                  activeColor: ColorPallet.pink,
                ),
              ],
            ),
            SizedBox(height: 8.0 * y),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translations.text('time'), style: settingText),
                InkWell(
                  onTap: () async {
                    if (Platform.isIOS) {
                      showModalBottomSheet(
                        context: context,
                        builder: (context) {
                          return CupertinoDatePicker(
                            onDateTimeChanged: (DateTime value) {
                              setState(
                                () {
                                  TimeOfDay notificationTime =
                                      TimeOfDay.fromDateTime(value);
                                  SettingsModel.of(context)
                                      .changeNotificationTime(
                                          context, notificationTime);
                                },
                              );
                            },
                            initialDateTime: DateTime.parse(
                                "2019-01-01 ${int.parse(model.hour) < 10 ? '0' + model.hour : model.hour}:${model.minute}"),
                            mode: CupertinoDatePickerMode.time,
                          );
                        },
                      );
                    } else {
                      TimeOfDay notificationTime = await showTimePicker(
                        context: context,
                        initialTime: TimeOfDay(
                            hour: int.parse(model.hour),
                            minute: int.parse(model.minute)),
                        builder: (BuildContext context, Widget child) {
                          return MediaQuery(
                            data: MediaQuery.of(context)
                                .copyWith(alwaysUse24HourFormat: true),
                            child: child,
                          );
                        },
                      );
                      SettingsModel.of(context)
                          .changeNotificationTime(context, notificationTime);
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.only(right: 9.5 * x),
                    child: Text(model.hour + ":" + model.minute,
                        style: model.status
                            ? TextStyle(
                                color: ColorPallet.pink,
                                fontSize: 16.0 * f,
                                fontWeight: FontWeight.w600)
                            : valueText),
                  ),
                ),
              ],
            ),
            SizedBox(height: 8.0 * y),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
          ],
        ),
      ),
    ));
  }
}

class _ContactWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingsModel>(
        builder: (context, child, model) {
      return _SettingsBoxWidget(
        settingsWidget: Container(
          key: keyButton2,
          margin:
              EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.forum,
                      color: ColorPallet.darkTextColor, size: 24.0 * x),
                  SizedBox(width: 10.0 * x),
                  Text(translations.text('contact'), style: headerText),
                ],
              ),
              SizedBox(height: 24.0 * x),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text('phoneNumber'), style: settingText),
                  InkWell(
                      onTap: () async {
                        String url = "tel:${translations.text('phoneNumberDigits')}";
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                      child: Padding(
                        padding: EdgeInsets.only(right: 9.5 * x),
                        child: Text(translations.text('phoneNumberDigits'),
                            style: valueTextBlue),
                      ))
                ],
              ),
              SizedBox(height: 8.0 * x),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
              SizedBox(height: 24.0 * y),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text('email'), style: settingText),
                  InkWell(
                      onTap: () async {
                        DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

                        String deviceInfoString;
                        if (Platform.isAndroid) {
                          AndroidDeviceInfo androidInfo =
                              await deviceInfo.androidInfo;
                          deviceInfoString = androidInfo.model;
                        } else if (Platform.isIOS) {
                          IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
                          deviceInfoString = iosInfo.model;
                        }
                        String url =
                            "mailto:${translations.text('emailAdress')}?subject=HBS App&body=\n\n\nSend from $deviceInfoString";
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                      child: Padding(
                        padding: EdgeInsets.only(right: 9.5 * x),
                        child:
                            Text(translations.text('emailAdress'), style: valueTextBlue),
                      ))
                ],
              ),
              SizedBox(height: 8.0 * y),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),              
            ],
          ),
        ),
      );
    });
  }
}

class _SettingsBoxWidget extends StatelessWidget {
  _SettingsBoxWidget({@required this.settingsWidget});

  final Widget settingsWidget;

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingsModel>(
        builder: (context, child, model) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0 * x),
        child: settingsWidget,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: ColorPallet.veryLightGray,
                offset: Offset(5.0 * x, 1.0 * y),
                blurRadius: 2.0 * x,
                spreadRadius: 3.0 * x)
          ],
        ),
      );
    });
  }
}

class _LanguageWidget extends StatefulWidget {
  @override
  __LanguageWidgetState createState() => __LanguageWidgetState();
}

class __LanguageWidgetState extends State<_LanguageWidget> {

  @override
  Widget build(BuildContext context) {
    SettingsModel.of(context).loadSettings();
    return _SettingsBoxWidget(
        settingsWidget: Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
      child: ScopedModelDescendant<SettingsModel>(
        builder: (context, child, model) => Column(
          key: keyButton3,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.language,
                    color: ColorPallet.darkTextColor, size: 24.0 * x),
                SizedBox(width: 10.0 * x),
                Text(translations.text('language'), style: headerText),
              ],
            ),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translations.text('language'), style: settingText),
                DropdownButton<String>(
                  value: International.languageFromId(LanguageSetting.key).name,
                  items: International.languages().map((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: Padding(
                        padding: EdgeInsets.only(right: 9.5 * x),
                        child: Text(value,
                            style: TextStyle(
                                color: ColorPallet.pink,
                                fontSize: 16.0 * f,
                                fontWeight: FontWeight.w600)),
                      ),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      LanguageSetting.of(context).language = International.languageFromName(newValue).id;
                      Login.saveLanguagePreference();
                    });
                  },
                ),
              ],
            ),
            SizedBox(height: 0.0 * y),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
          ],
        ),
      ),
    ));
  }
}

class _VersionWidget extends StatelessWidget {
  Future<String> getVersionNumber() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String versionName = packageInfo.version;
    String versionCode = packageInfo.buildNumber;
    return "$versionName+$versionCode";
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingsModel>(
        builder: (context, child, model) {
      return _SettingsBoxWidget(
        settingsWidget: Container(
          margin:
              EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.info_outline,
                      color: ColorPallet.darkTextColor, size: 24.0 * x),
                  SizedBox(width: 10.0 * x),
                  Text(translations.text('information'), style: headerText),
                ],
              ),
              SizedBox(height: 24.0 * x),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text('versionNumber'), style: settingText),
                  Padding(
                    padding: EdgeInsets.only(right: 9.5 * x),
                    child: FutureBuilder(
                      future: getVersionNumber(),
                      builder: (BuildContext context,
                          AsyncSnapshot<String> snapshot) {
                        if (!snapshot.hasData) {
                          return Text("");
                        } else {
                          return Text(snapshot.data, style: settingText);
                        }
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8.0 * x),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
            ],
          ),
        ),
      );
    });
  }
}
