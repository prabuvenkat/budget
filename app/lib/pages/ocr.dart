import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:budget_onderzoek/resources/ssb_color_palett.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class OCR extends StatefulWidget {
  final String imagePath;

  const OCR(this.imagePath): super();

  @override
  _OCR createState() => _OCR();
}

class _OCR extends State<OCR> {
  static const String TESS_DATA_CONFIG = 'assets/tessdata_config.json';
  static const String TESS_DATA_PATH = 'assets/tessdata';
  static const MethodChannel _channel = const MethodChannel('tesseract_ocr');

  String _text = '--';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    try {
      final ocrText = await extractText(widget.imagePath);
      // final ocrText = await TesseractOcr.extractText(widget.imagePath, language: 'eng');
      setState(() {
        _text = ocrText;
      });
    } on Exception catch (e) {
      setState(() {
        _text = '$e';
      });
    }
  }

  Future<String> extractText(String imagePath, {String language}) async {
    assert(await File(imagePath).exists(), true);
    final String tessData = await _loadTessData();
    final String extractText = await _channel.invokeMethod('extractText', <String, dynamic>{
      'imagePath': imagePath,
      'tessData': tessData,
      'language': 'eng',
    });
    return extractText;
  }

  static Future<String> _loadTessData() async {
    final Directory appDirectory = await getApplicationDocumentsDirectory();
    final String tessdataDirectory = join(appDirectory.path, 'tessdata');

    if (!await Directory(tessdataDirectory).exists()) {
      await Directory(tessdataDirectory).create();
    }
    await _copyTessDataToAppDocumentsDirectory(tessdataDirectory);
    return appDirectory.path;
  }

  static Future _copyTessDataToAppDocumentsDirectory(String tessdataDirectory) async {
    final String config = await rootBundle.loadString(TESS_DATA_CONFIG);
    Map<String, dynamic> files = jsonDecode(config);
    for (var file in files["files"]) {
      if (!await File('$tessdataDirectory/$file').exists()) {
        final ByteData data = await rootBundle.load('$TESS_DATA_PATH/$file');
        final Uint8List bytes = data.buffer.asUint8List(
          data.offsetInBytes,
          data.lengthInBytes,
        );
        await File('$tessdataDirectory/$file').writeAsBytes(bytes);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('OCR'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
                  Center(
                    child: Text(widget.imagePath,
                        style: TextStyle(color: SSBColorPalette.GREEN_4)),
                  ),
                  Divider(color: Colors.grey),
                  Text('Error: ',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Center(
                    child: Text('$_text',
                        style: TextStyle(color: SSBColorPalette.RED_3)),
                  ),
                ],
              ),
            )
      )
    );
  }
}
