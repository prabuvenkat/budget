import 'dart:async';

import 'package:budget_onderzoek/util/algorithms/string_matching_products.dart';
import 'package:budget_onderzoek/util/algorithms/string_matching_shops.dart';
import 'package:budget_onderzoek/database/datamodels/search_suggestion_db.dart';
import 'package:budget_onderzoek/database/datamodels/transaction_db.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/scoped_models/settings_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/util/user_input.dart';
import 'package:budget_onderzoek/widgets/draggable_scrollbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:budget_onderzoek/translations/translations.dart';


Translations translations;

class SearchWidget extends StatefulWidget {
  SearchWidget(this.isProductSearch);

  final bool isProductSearch;

  @override
  State<StatefulWidget> createState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: ColorPallet.pink,
    ));
    return __SearchWidgetState();
  }
}

class __SearchWidgetState extends State<SearchWidget> {
  FocusNode myFocusNode;
  List<Map<String, dynamic>> results;
  String searchString = "";

  final TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }

  Future<List<Map<String, dynamic>>> getSearchResults() async {
    if (widget.isProductSearch) {
      results = await StringMatchingProducts.getSearchResults(searchString);
    } else {
      results = await StringMatchingShops.getSearchResults(searchString);
    }

    return results;
  }

  bool inputChecker() {
    searchString = UserInput.textSanitizer(searchString);
    if (!UserInput.textValidator(searchString) || searchString.length == 0) {
      Toast.show(translations.text ('invalidInput'), context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      setState(() {
        _controller.clear();
        FocusScope.of(context).requestFocus(myFocusNode);
      });
      return false;
    } else {
      return true;
    }
  }

  String formatSearchString(String searchStringTest) {
    if (searchStringTest == null) {
      return "";
    }
    if (searchStringTest.length > 0) {
      return searchString[0].toUpperCase() + searchString.substring(1);
    } else {
      return "";
    }
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Search');
    ScrollController _arrowsController = ScrollController();
    return WillPopScope(
      onWillPop: () {
        ScopedModel.of<NewTransactionModel>(context).product = null;
        SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(statusBarColor: ColorPallet.pink),
        );
        return Future.value(true);
      },
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Positioned(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      color: ColorPallet.pink,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              child: SafeArea(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 73.0 * y,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          border: Border.all(
                              width: 13.0 * x, color: ColorPallet.pink),
                        ),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: TextField(
                                  autocorrect: false,
                                  focusNode: myFocusNode,
                                  keyboardType: TextInputType.text,
                                  onSubmitted: (value) {},
                                  onChanged: (value) {
                                    setState(() {
                                      searchString = value;
                                    });
                                  },
                                  autofocus: true,
                                  controller: _controller,
                                  style: TextStyle(
                                      color: ColorPallet.darkTextColor,
                                      fontSize: 20.0 * f,
                                      fontWeight: FontWeight.w500),
                                  decoration: InputDecoration(
                                    hintText: widget.isProductSearch
                                        ? translations.text ('findProductService')
                                        : translations.text ('findShop'),
                                    hintStyle: TextStyle(
                                        color: ColorPallet.midGray,
                                        fontSize: 18.0 * f),
                                    prefixIcon: InkWell(
                                      child: Icon(
                                        Icons.arrow_back,
                                        color: ColorPallet.midGray,
                                        size: 24 * x,
                                      ),
                                      onTap: () {
                                        SystemChrome.setSystemUIOverlayStyle(
                                          SystemUiOverlayStyle(
                                            statusBarColor: ColorPallet.pink,
                                          ),
                                        );
                                        ScopedModel.of<NewTransactionModel>(
                                                context)
                                            .product = null;
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                            ),
                            searchString.length == 0
                                ? Container()
                                : InkWell(
                                    child: Container(
                                      width: 110 * x,
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 10 * x, vertical: 10 * y),
                                      decoration: BoxDecoration(
                                          color: ColorPallet.pink,
                                          borderRadius:
                                              BorderRadius.circular(10 * x),
                                          boxShadow: [
                                            BoxShadow(
                                              color: ColorPallet.lightGray,
                                              offset: Offset(1.0 * x, 1.0 * y),
                                              blurRadius: 1.5 * x,
                                              spreadRadius: 1.5 * x,
                                            )
                                          ]),
                                      child: Center(
                                        child: Text(translations.text ('notFound') + "?",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 13 * f,
                                                fontWeight: FontWeight.w500)),
                                      ),
                                    ),
                                    onTap: () {
                                      if (inputChecker()) {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return _SearchNotFoundDialog(
                                                  searchString,
                                                  results,
                                                  widget.isProductSearch);
                                            });
                                      }
                                    },
                                  ),
                          ],
                        ),
                      ),
                      SizedBox(height: 4 * y),
                      Expanded(
                        child: Listener(
                          onPointerDown: (v) {
                            myFocusNode.unfocus();
                          },
                          child: FutureBuilder(
                            future: getSearchResults(),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<Map<String, dynamic>>>
                                    snapshot) {
                              if (!snapshot.hasData ||
                                  snapshot.data.length == 0 ||
                                  searchString.length == 0) {
                                return _QuickSuggestionList(
                                    widget.isProductSearch);
                              }
                              return DraggableScrollbar.rrect(
                                alwaysVisibleScrollThumb: true,
                                heightScrollThumb: 50.0,
                                backgroundColor:
                                    Colors.grey, //ColorPallet.primaryColor,
                                padding: EdgeInsets.only(right: 4.0),
                                controller: _arrowsController,
                                child: ListView.builder(
                                  controller: _arrowsController,
                                  scrollDirection: Axis.vertical,
                                  itemCount: snapshot.data.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return _SearchResultTile(
                                        snapshot.data[index]['examples'][0]
                                                .toString()[0]
                                                .toUpperCase() +
                                            snapshot.data[index]['examples'][0]
                                                .toString()
                                                .substring(1),
                                        snapshot.data[index]['category']
                                            .toString(),
                                        widget.isProductSearch,
                                        false);
                                  },
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _QuickSuggestionList extends StatelessWidget {
  // deze moet ik aanpassen
  _QuickSuggestionList(this.isProductSearch);

  final bool isProductSearch;

  Future<List<Map<String, dynamic>>> getSearchResults(
      BuildContext context, bool mostRecentSearchSuggestion) async {
    if (isProductSearch) {
      if (mostRecentSearchSuggestion) {
        return await SearchSuggestions.getMostRecentProducts();
      } else {
        return await SearchSuggestions.getMostFrequentProducts();
      }
    } else {
      if (mostRecentSearchSuggestion) {
        return await SearchSuggestions.getMostRecentStores();
      } else {
        return await SearchSuggestions.getMostFrequentStores();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    ScrollController _arrowsController = ScrollController();
    return ScopedModelDescendant<SettingsModel>(
      builder: (context, child, model) => FutureBuilder(
        future: getSearchResults(
            context,
            isProductSearch
                ? model.mostRecentSearchSuggestionProducts
                : model.mostRecentSearchSuggestionStore),
        builder: (BuildContext context,
            AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
          if (!snapshot.hasData || snapshot.data.length == 0) {
            return Container();
          }
          return DraggableScrollbar.rrect(
            alwaysVisibleScrollThumb: true,
            heightScrollThumb: 50.0,
            backgroundColor: Colors.grey, // ColorPallet.primaryColor,
            padding: EdgeInsets.only(right: 4.0),
            controller: _arrowsController,
            child: ListView.builder(
              controller: _arrowsController,
              scrollDirection: Axis.vertical,
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                if (isProductSearch) {
                  return _SearchResultTile(
                      snapshot.data[index]['product']
                              .toString()[0]
                              .toUpperCase() +
                          snapshot.data[index]['product']
                              .toString()
                              .substring(1),
                      snapshot.data[index]['productCategory'].toString(),
                      isProductSearch,
                      index == 0);
                } else {
                  return _SearchResultTile(
                      snapshot.data[index]['storeName']
                              .toString()[0]
                              .toUpperCase() +
                          snapshot.data[index]['storeName']
                              .toString()
                              .substring(1),
                      snapshot.data[index]['storeType'].toString(),
                      isProductSearch,
                      index == 0);
                }
              },
            ),
          );
        },
      ),
    );
  }
}

class _SearchResultTile extends StatelessWidget {
  const _SearchResultTile(this.headerText, this.subText, this.isProductSearch,
      this.displaySearchSuggestionOption);

  final bool displaySearchSuggestionOption;
  final String headerText;
  final bool isProductSearch;
  final String subText;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        displaySearchSuggestionOption
            ? Container(
                height: 40 * y,
                width: MediaQuery.of(context).size.width,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ScopedModelDescendant<SettingsModel>(
                        builder: (context, child, model) => InkWell(
                            onTap: () {
                              if (isProductSearch) {
                                model.changeSearchSuggestionTypeProducts();
                              } else {
                                model.changeSearchSuggestionTypeStore();
                              }
                            },
                            child: Row(
                              children: <Widget>[
                                Text(
                                  translations.text ('recentItems'),
                                  style: TextStyle(
                                      color: (isProductSearch &&
                                                  model
                                                      .mostRecentSearchSuggestionProducts) ||
                                              (!isProductSearch &&
                                                  model
                                                      .mostRecentSearchSuggestionStore)
                                          ? ColorPallet.pink
                                          : ColorPallet.midGray,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16 * f),
                                ),
                                SizedBox(width: 3 * x),
                                Icon(
                                  Icons.arrow_downward,
                                  color: (isProductSearch &&
                                              model
                                                  .mostRecentSearchSuggestionProducts) ||
                                          (!isProductSearch &&
                                              model
                                                  .mostRecentSearchSuggestionStore)
                                      ? ColorPallet.pink
                                      : ColorPallet.midGray,
                                  size: 15 * x,
                                ),
                                SizedBox(width: 30 * x),
                                Text(
                                  translations.text ('frequentItems'),
                                  style: TextStyle(
                                      color: (isProductSearch &&
                                                  model
                                                      .mostRecentSearchSuggestionProducts) ||
                                              (!isProductSearch &&
                                                  model
                                                      .mostRecentSearchSuggestionStore)
                                          ? ColorPallet.midGray
                                          : ColorPallet.pink,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16 * f),
                                ),
                                SizedBox(width: 3 * x),
                                Icon(
                                  Icons.arrow_downward,
                                  color: (isProductSearch &&
                                              model
                                                  .mostRecentSearchSuggestionProducts) ||
                                          (!isProductSearch &&
                                              model
                                                  .mostRecentSearchSuggestionStore)
                                      ? ColorPallet.midGray
                                      : ColorPallet.pink,
                                  size: 15 * x,
                                ),
                              ],
                            )),
                      ),
                    ]),
              )
            : Container(),
        InkWell(
          onTap: () {
            if (isProductSearch) {
              ScopedModel.of<NewTransactionModel>(context).product = headerText;
              ScopedModel.of<NewTransactionModel>(context).productCategory =
                  subText;
            } else {
              SearchSuggestions.addStore(headerText, subText);
              ScopedModel.of<NewTransactionModel>(context).store = headerText;
              ScopedModel.of<NewTransactionModel>(context).storeType = subText;
            }
            NewTransactionModel.of(context).isProductInfoComplete();
            NewTransactionModel.of(context).isReceiptInfoComplete();
            Navigator.of(context).pop();
          },
          
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: 17 * x, vertical: 2 * y),
                      child: Text(
                        headerText,
                        style: TextStyle(
                            fontSize: 16 * f,
                            fontWeight: FontWeight.w700,
                            color: ColorPallet.darkTextColor),
                      ),
                    ),
                    SizedBox(height: 3 * y),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 17 * x),
                      child: Text(
                        subText,
                        style: TextStyle(
                            fontSize: 12.5 * f,
                            color: ColorPallet.darkTextColor.withOpacity(0.8),
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(height: 3 * y),
                  ],
                ),
              ),
              
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 17 * x, vertical: 2 * y),
          width: MediaQuery.of(context).size.width,
          height: 1 * y,
          color: ColorPallet.lightGray,
        )
      ],
    );
  }
}

class _SearchNotFoundDialog extends StatefulWidget {
  _SearchNotFoundDialog(this.searchString, this.results, this.isProductSearch);

  final bool isProductSearch;
  final List<Map<String, dynamic>> results;
  final String searchString;

  @override
  __SearchNotFoundDialogState createState() => __SearchNotFoundDialogState();
}

class __SearchNotFoundDialogState extends State<_SearchNotFoundDialog> {
  List<String> categoryOptions = [];
  String selectedCategory;

  @override
  void initState() {
    super.initState();
    widget.results.forEach((productInfo) {
      if (!(categoryOptions.contains(productInfo['category']) ||
          productInfo['category'] == "Unknown")) {
        if (categoryOptions.length < 4) {
          categoryOptions.add(productInfo['category']);
        }
      }
    });
    categoryOptions.add(translations.text ('additional2'));
    selectedCategory = categoryOptions[0];
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.all(0),
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              color: ColorPallet.pink,
              height: 68 * y,
              width: 400 * x,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 20 * y),
                  Padding(
                    padding: EdgeInsets.only(left: 35.0 * x),
                    child: Text(translations.text ('addYourself'),
                        style: TextStyle(
                            fontSize: 23 * f,
                            fontWeight: FontWeight.w500,
                            color: Colors.white)),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 20),
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 15 * y,
                    width: 400 * x,
                  ),
                  Text(
                    widget.isProductSearch
                        ? translations.text ('productService')
                        : translations.text ('shop'),
                    style: TextStyle(
                      fontSize: 14 * f,
                      fontWeight: FontWeight.w500,
                      color: ColorPallet.darkTextColor.withOpacity(0.8),
                    ),
                  ),
                  SizedBox(height: 5 * y),
                  Text(
                    widget.searchString[0].toUpperCase() +
                        widget.searchString.substring(1),
                    style: TextStyle(
                      fontSize: 20 * f,
                      fontWeight: FontWeight.w600,
                      color: ColorPallet.darkTextColor,
                    ),
                  ),
                  SizedBox(
                    height: 15 * y,
                    width: 400 * x,
                  ),
                  Text(
                    widget.isProductSearch
                        ? translations.text ('belongsTo') + ":"
                        : translations.text ('belongsTo2') + ":",
                    style: TextStyle(
                      fontSize: 14 * f,
                      fontWeight: FontWeight.w500,
                      color: ColorPallet.darkTextColor.withOpacity(0.8),
                    ),
                  ),
                  SizedBox(height: 10 * y),
                  Column(
                    children: <Widget>[
                      Column(
                        children: categoryOptions.map(
                          (String category) {
                            return RadioListTile(
                              dense: true,
                              value: category,
                              groupValue: selectedCategory,
                              title: Text(
                                category,
                                style: TextStyle(
                                  fontSize: 14 * f,
                                  color: ColorPallet.darkTextColor,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              onChanged: (val) {
                                category = categoryOptions[0];
                                setState(() {
                                  selectedCategory = val;
                                });
                              },
                              activeColor: ColorPallet.pink,
                            );
                          },
                        ).toList(),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          FlatButton(
                            child: Text(translations.text ('cancel'),
                                style: TextStyle(
                                  color: ColorPallet.pink,
                                  fontSize: 14 * f,
                                )),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          FlatButton(
                            child: Text(translations.text ('add'),
                                style: TextStyle(
                                  color: ColorPallet.pink,
                                  fontSize: 14 * f,
                                )),
                            onPressed: () {
                              String example =
                                  widget.searchString[0].toUpperCase() +
                                      widget.searchString.substring(1);

                              if (widget.isProductSearch) {
                                TransactionDatabase.addNewProductOption(
                                    example, selectedCategory);

                                ScopedModel.of<NewTransactionModel>(context)
                                    .product = example;
                                ScopedModel.of<NewTransactionModel>(context)
                                    .productCategory = selectedCategory;
                              } else {
                                SearchSuggestions.addStore(example, selectedCategory);
                                TransactionDatabase.addNewShopOption(
                                    example, selectedCategory);

                                ScopedModel.of<NewTransactionModel>(context)
                                    .store = example;
                                ScopedModel.of<NewTransactionModel>(context)
                                    .storeType = selectedCategory;
                              }
                              NewTransactionModel.of(context)
                                  .isProductInfoComplete();
                              NewTransactionModel.of(context)
                                  .isReceiptInfoComplete();
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}
