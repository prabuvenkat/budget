import 'dart:async';
import 'dart:io';

import 'package:budget_onderzoek/resources/ssb_color_palett.dart';
import 'package:budget_onderzoek/translations/translations.dart';
import 'package:budget_onderzoek/translations/international.dart';
import 'package:budget_onderzoek/util/algorithms/quick_search.dart';
import 'package:budget_onderzoek/util/algorithms/string_matching_shops.dart';
import 'package:budget_onderzoek/pages/camera.dart';
import 'package:budget_onderzoek/pages/search.dart';
import 'package:budget_onderzoek/resources/color_pallet.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/util/custom_icons.dart';
import 'package:budget_onderzoek/util/textfield_workaround.dart';
import 'package:budget_onderzoek/util/user_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';
import 'package:budget_onderzoek/util/responsive_ui.dart';
import 'package:budget_onderzoek/widgets/custom_flutter/custom_dialog.dart'
    as customDialog;
import 'package:budget_onderzoek/widgets/close_page_warning_dialog.dart';
import 'package:budget_onderzoek/pages/ocr.dart';

Translations translations;

class ReceiptDetailsPage extends StatelessWidget {
  Future<void> loadDatabases() async {
    QuickSearch.loadQuickSearchTable();
    StringMatchingShops.loadShopTable();
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Camera_Entry');
    loadDatabases();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: ColorPallet.pink,
    ));

    return WillPopScope(
      onWillPop: () async {
        await showDialog(
          context: context,
          builder: (BuildContext context) {
            return ClosingPageWarning();
          },
        );
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: InkWell(
            child: Icon(Icons.close, size: 28 * y),
            onTap: () async {
              await showDialog(
                context: context,
                builder: (BuildContext context) {
                  return ClosingPageWarning();
                },
              );
            },
          ),
          titleSpacing: 0,
          backgroundColor: ColorPallet.pink,
          title: Row(
            children: <Widget>[
              Text(translations.text ('addInfo'), style: TextStyle(fontSize: 21 * x)),
              Expanded(child: Container()),
              ScopedModelDescendant<NewTransactionModel>(
                builder: (context, child, model) => Container(
                  width: 112 * x,
                  height: 30 * y,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0 * x),
                    ),
                    color: model.receiptComplete
                        ? ColorPallet.darkTextColor
                        : ColorPallet.midGray,
                    child: Text(translations.text ('complete'),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 16.0 * f)),
                    onPressed: () async {
                      if (model.store != "" && model.receiptPrice != "") {
                        model.receiptPrice = double.parse(
                                model.receiptPrice.replaceAll(',', '.'))
                            .toStringAsFixed(2);
                        model.addNewTransAction();
                        SystemChrome.setSystemUIOverlayStyle(
                          SystemUiOverlayStyle(
                              statusBarColor: ColorPallet.primaryColor),
                        );
                        Navigator.of(context).pop();
                      } else {
                        String warning = translations.text ('pictureInfoIncomplete');
                        await showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                titlePadding: EdgeInsets.all(0),
                                title: Container(
                                  color: ColorPallet.pink,
                                  padding: EdgeInsets.all(20),
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        translations.text ('informationIncomplete'),
                                        style: TextStyle(
                                            fontSize: 20 * f,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.white),
                                      ),
                                    ],
                                  ),
                                ),
                                content: Text(warning,
                                    style: TextStyle(
                                        fontSize: 17 * f,
                                        fontWeight: FontWeight.w500,
                                        color: ColorPallet.darkTextColor)),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text(translations.text ('ok'),
                                        style:
                                            TextStyle(color: ColorPallet.pink)),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  )
                                ],
                              );
                            });
                      }
                    },
                  ),
                ),
              ),
              SizedBox(
                width: 25 * x,
              )
            ],
          ),
        ),
        body: Theme(
          data: ThemeData(
            primaryColor: ColorPallet.pink,
            accentColor: ColorPallet.pink,
          ),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _StoreSelectionList(),
                SizedBox(height: 10 * y),
                Stack(
                  children: <Widget>[
                    // Image.asset(
                    //   NewTransactionModel.of(context).receiptLocation,
                    // ),
                    Image.file(
                      File(NewTransactionModel.of(context).receiptLocation),
                    ),
                    Positioned(
                      top: 25,
                      right: 25,
                      child: FloatingActionButton(
                        heroTag: 'image',
                        child: Icon(Icons.camera_alt, color: Colors.white),
                        backgroundColor: ColorPallet.pink,
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => CameraPage(true),
                            ),
                          );
                        },
                      ),
                    ),
                    Positioned(
                      top:100,
                      right: 25,
                      child: FloatingActionButton(
                        heroTag: 'ocr',
                        child: Icon(Icons.text_fields, color: Colors.white),
                        backgroundColor: SSBColorPalette.GREEN_4,
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => OCR(NewTransactionModel.of(context).receiptLocation),
                            )
                          );
                        },
                      )
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _StoreSelectionList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 12 * y),
      width: 400 * x,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(5.0 * x),
        ),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: ColorPallet.veryLightGray,
              offset: Offset(5.0 * y, 1.0 * x),
              blurRadius: 2.0 * x,
              spreadRadius: 3.0 * x)
        ],
      ),
      child: ScopedModelDescendant<NewTransactionModel>(
        builder: (context, child, model) => Column(
          children: <Widget>[
            SizedBox(height: 20 * y, width: MediaQuery.of(context).size.width),
            _StoreInput(),
            _DateInput(),
            SizedBox(height: 7 * y),
            _PriceInput(),
            SizedBox(height: 9 * y),
            _ProductTypeInput(),
          ],
        ),
      ),
    );
  }
}

class _StoreInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0 * x)),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 10.0 * x, vertical: 10.0 * y),
              child: InkWell(
                onTap: () async {
                  String result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SearchWidget(false),
                    ),
                  );
                  if (result != null) {
                    newTransaction.store = result;
                    newTransaction.isReceiptInfoComplete();
                  }
                },
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.store,
                            color: ColorPallet.darkTextColor, size: 27 * x),
                        SizedBox(width: 10.0 * x),
                        Container(
                          width: 305 * x,
                          child: Text(
                              newTransaction.store != ""
                                  ? newTransaction.store
                                  : translations.text ('enterShop'),
                              style: TextStyle(
                                  color: newTransaction.store != ""
                                      ? ColorPallet.darkTextColor
                                      : ColorPallet.midGray,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18 * f),
                              overflow: TextOverflow.ellipsis),
                        )
                      ],
                    ),
                    newTransaction.storeType == ""
                        ? Container()
                        : Column(
                            children: <Widget>[
                              SizedBox(height: 5 * y),
                              Row(
                                children: <Widget>[
                                  Icon(Icons.category,
                                      color: ColorPallet.darkTextColor,
                                      size: 27 * x),
                                  SizedBox(width: 10.0 * x),
                                  Container(
                                    width: 305 * x,
                                    child: Text(newTransaction.storeType,
                                        style: TextStyle(
                                            color: ColorPallet.darkTextColor
                                                .withOpacity(0.7),
                                            fontWeight: FontWeight.w500,
                                            fontSize: 18 * f),
                                        overflow: TextOverflow.ellipsis),
                                  )
                                ],
                              ),
                            ],
                          ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              translations.text ('shop'),
              style: TextStyle(
                  color: ColorPallet.darkTextColor,
                  fontWeight: FontWeight.w700,
                  fontSize: 14 * f),
            ),
          ),
        ),
      ],
    );
  }
}

class _DateInput extends StatefulWidget {
  @override
  __DateInputState createState() => __DateInputState();
}

class __DateInputState extends State<_DateInput> {
  String hintText = translations.text ('today');
  DateTime initialDate =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

  NewTransactionModel newTransaction;

  Future<Null> _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: DateTime(2019),
      lastDate: DateTime(2101),
    );
    picked = DateTime(picked.year, picked.month, picked.day);
    if (picked != null) {
      newTransaction.date = DateFormat('yyyy-MM-dd').format(picked);
      newTransaction.notifyListeners();
    }
  }

  String getDateString(String date) {
    DateTime today =
        DateTime(initialDate.year, initialDate.month, initialDate.day);
    DateTime yesterday =
        DateTime(initialDate.year, initialDate.month, initialDate.day - 1);
    DateTime tomorrow =
        DateTime(initialDate.year, initialDate.month, initialDate.day + 1);

    if (date == DateFormat('yyyy-MM-dd').format(today)) {
      return translations.text ('today');
    } else if (date == DateFormat('yyyy-MM-dd').format(yesterday)) {
      return translations.text ('yesterday');
    } else if (date == DateFormat('yyyy-MM-dd').format(tomorrow)) {
      return translations.text ('tomorrow');
    } else {
      return DateFormat('EEEE, d MMMM', International.languageFromId(LanguageSetting.key).localeKey)
          .format(DateTime.parse(date));
    }
  }

  @override
  Widget build(BuildContext context) {
    newTransaction = NewTransactionModel.of(context);
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0 * x)),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 10.0 * x, vertical: 10.0 * y),
              child: InkWell(
                onTap: () async {
                  _selectDate(context);
                },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.today,
                        color: ColorPallet.darkTextColor, size: 27 * x),
                    SizedBox(width: 10.0 * x),
                    ScopedModelDescendant<NewTransactionModel>(
                      builder: (context, child, model) => Text(
                          getDateString(model.date),
                          style: TextStyle(
                              color: ColorPallet.darkTextColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 18 * f)),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(translations.text ('date'),
                style: TextStyle(
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w700,
                    fontSize: 14 * f)),
          ),
        ),
      ],
    );
  }
}

class _PriceInput extends StatefulWidget {
  @override
  __PriceInputState createState() => __PriceInputState();
}

class __PriceInputState extends State<_PriceInput> {
  String price = "";
  TextEditingControllerWorkaroud textController =
      TextEditingControllerWorkaroud();

  @override
  void dispose() {
    super.dispose();
    textController.dispose();
  }

  void checkUserInput(String newPrice, BuildContext context) {
    //newPrice = UserInput.valueSanitizer(newPrice);
    // if (newPrice.contains(",")) {
    //   newPrice = newPrice.replaceAll(",", ".");
    //   textController.setTextAndPosition(newPrice);
    // }

    if (!UserInput.valueValidator(newPrice)) {
      Toast.show(translations.text ('invalidAmount'), context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      textController.setTextAndPosition(price);
    } else {
      if (newPrice == ".") {
        price = "0.";
      } else if (newPrice == ",") {
        price = "0,";
      } else {
        price = newPrice;
      }
      NewTransactionModel.of(context).price = price;
      NewTransactionModel.of(context).receiptPrice = price;
      NewTransactionModel.of(context).isReceiptInfoComplete();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(0.0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0 * x)),
            child: Row(
              children: <Widget>[
                SizedBox(width: 10.0 * x),
                Icon(Icons.local_offer,
                    color: ColorPallet.darkTextColor, size: 27 * x),
                // SizedBox(width: 3.0),
                Container(
                  alignment: Alignment.centerLeft,
                  width: 320 * x,
                  height: 38 * y,
                  margin: EdgeInsets.only(top: 7 * y),
                  child: TextField(
                    controller: textController,
                    style: TextStyle(
                        color: ColorPallet.darkTextColor,
                        fontWeight: FontWeight.w500,
                        fontSize: 18 * f),
                    autocorrect: false,
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    onChanged: (newPrice) {
                      setState(() {
                        checkUserInput(newPrice, context);
                      });
                    },
                    onSubmitted: (value) {
                      if (NewTransactionModel.of(context).receiptPrice != "") {
                        price = double.parse(price.replaceAll(',', '.'))
                            .toStringAsFixed(2);
                        textController.text = price;
                        NewTransactionModel.of(context).receiptPrice = price;
                        NewTransactionModel.of(context).isReceiptInfoComplete();
                      }
                    },
                    decoration: InputDecoration(
                        hintText:
                            NewTransactionModel.of(context).receiptPrice != ""
                                ? double.parse(NewTransactionModel.of(context)
                                        .receiptPrice
                                        .replaceAll(',', '.'))
                                    .toStringAsFixed(2)
                                : translations.text ('enterTotalPrice'),
                        hintStyle: TextStyle(
                            color: ColorPallet.midGray,
                            fontWeight: FontWeight.w500,
                            fontSize: 18 * f),
                        filled: true,
                        fillColor: Colors.transparent,
                        border: InputBorder.none,
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 10 * x)),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0),
            color: Colors.white,
            child: Text(
              translations.text ('totalPrice'),
              style: TextStyle(
                fontSize: 14 * f,
                color: ColorPallet.darkTextColor,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _ProductTypeInput extends StatefulWidget {
  @override
  __ProductTypeInputState createState() => __ProductTypeInputState();
}

class __ProductTypeInputState extends State<_ProductTypeInput> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0 * x)),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 10.0 * x, vertical: 10.0 * y),
              child: InkWell(
                onTap: () async {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return customDialog.AlertDialog(
                          contentPadding: EdgeInsets.all(0),
                          content: Container(
                            width: 500 * x,
                            height: 540 * f,
                            child: Center(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    height: 75 * y,
                                    color: ColorPallet.pink,
                                    child: Row(
                                      children: <Widget>[
                                        SizedBox(width: 20 * x),
                                        Text(
                                          translations.text ('productCategory'),
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 24 * f,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(),
                                        ),
                                        InkWell(
                                          onTap: () async {
                                            Navigator.pop(context);
                                          },
                                          child: Icon(
                                            Icons.close,
                                            color: Colors.white,
                                            size: 30 * x,
                                          ),
                                        ),
                                        SizedBox(width: 20 * x),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 8.0 * x),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              CategoryButton(
                                                  Icons.healing,
                                                  translations.text ('health'),
                                                  30 * x,
                                                  '06'),
                                              CategoryButton(
                                                  Icons.phone,
                                                  translations.text ('communication'),
                                                  30 * x,
                                                  '08'),
                                              CategoryButton(
                                                  FontAwesomeIcons.square,
                                                  translations.text ('otherCategory'),
                                                  29 * x,
                                                  '12'),
                                            ],
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              CategoryButton(
                                                  Icons.local_laundry_service,
                                                  translations.text ('householdExpenses'),
                                                  31 * x,
                                                  '05'),
                                              CategoryButton(
                                                  CustomIcons.utilities,
                                                  translations.text ('housingCost'),
                                                  31 * x,
                                                  '04'),
                                              CategoryButton(
                                                  Icons.school,
                                                  translations.text ('education'),
                                                  31 * x,
                                                  '10'),
                                            ],
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              CategoryButton(
                                                  FontAwesomeIcons.tshirt,
                                                  translations.text ('clothing'),
                                                  28 * x,
                                                  '03'),
                                              CategoryButton(
                                                  Icons.hotel,
                                                  translations.text ('restaurantAndHotel'),
                                                  31 * x,
                                                  '11'),
                                              CategoryButton(
                                                  Icons.local_movies,
                                                  translations.text ('recreationAndCultur'),
                                                  30 * x,
                                                  '09'),
                                            ],
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              CategoryButton(
                                                  Icons.local_bar,
                                                  translations.text ('alcoholAndTobacco'),
                                                  31 * x,
                                                  '02'),
                                              CategoryButton(
                                                  Icons.local_dining,
                                                  translations.text ('foodAndDrink'),
                                                  31 * x,
                                                  '01'),
                                              CategoryButton(
                                                  Icons.local_taxi,
                                                  translations.text ('transport'),
                                                  31 * x,
                                                  '07'),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.shopping_cart,
                        color: ColorPallet.darkTextColor, size: 27 * x),
                    SizedBox(width: 10.0 * x),
                    ScopedModelDescendant<NewTransactionModel>(
                      builder: (context, child, model) => Container(
                        width: 305 * x,
                        child: Text(
                            model.receiptProductTypeString == ""
                                ? translations.text ('enterTypeOfProducts')
                                : model.receiptProductTypeString,
                            style: TextStyle(
                                color: model.receiptProductTypeString == ""
                                    ? ColorPallet.midGray
                                    : ColorPallet.darkTextColor,
                                fontWeight: FontWeight.w500,
                                fontSize: 18 * f),
                            overflow: TextOverflow.ellipsis),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              translations.text ('productCategory'),
              style: TextStyle(
                  color: ColorPallet.darkTextColor,
                  fontWeight: FontWeight.w700,
                  fontSize: 14 * f),
            ),
          ),
        ),
      ],
    );
  }
}

class CategoryButton extends StatelessWidget {
  CategoryButton(
      this.iconData, this.text, this.iconSize, this.mainCategoryLookupValue);

  final IconData iconData;
  final double iconSize;
  final String mainCategoryLookupValue;
  final String text;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        NewTransactionModel.of(context)
            .updateReceiptProductType(text, mainCategoryLookupValue);
        Navigator.of(context).pop(context);
      },
      child: Container(
        width: 100 * x,
        height: 90 * y,
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(height: 5 * y),
              Icon(
                iconData,
                color: ColorPallet.darkTextColor,
                size: iconSize,
              ),
              SizedBox(height: 8 * y),
              Text(
                text,
                style: TextStyle(
                    color: ColorPallet.darkTextColor,
                    fontSize: 13 * f,
                    fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
