import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class FilterModel extends Model {
  String endDate = "";
  String maximumPrice = "";
  String minimumPrice = "";
  String startDate = "";

  void reset() {
    minimumPrice = "";
    maximumPrice = "";
    startDate = "";
    endDate = "";
    notifyListeners();
  }

  static FilterModel of(BuildContext context) =>
      ScopedModel.of<FilterModel>(context);
}
