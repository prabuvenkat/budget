import 'package:flutter/material.dart';

// Ref: https://statisticsnorway.github.io/design-system/#/components/color
class SSBColorPalette {
  // Primary Colors
  static const Color DARK_5 = Color(0xFF274247);
  static const Color GREEN_4 = Color(0xFF00824D);
  static const Color GREEN_1 = Color(0xFFECFEED);
  static const Color WHITE = Color(0xFFFFFFFF);

  // Functional Colors
  static const Color DARK_6 = Color(0xFF162327);
  static const Color RED_3 = Color(0xFFED5935);
  static const Color BLUE_3 = Color(0xFF3396D2);
}
